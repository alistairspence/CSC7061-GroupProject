package master;

import java.sql.*;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * The DBHelper class contains methods for reading from and writing to the
 * database, as well as utility methods for checking initialisation status and
 * clearing the database.
 * 
 * @author <i>Alistair Spence
 * @author Caolan Daly
 * @author Cathal McNulty
 * @author John Corry
 * @author Niall Logue</i>
 *
 */
public class DBHelper {

	// con: connection available to all methods in this class when necessary.
	public static Connection con;

	// NUMBER_OF_TEAMS: sets the max number of teams that can be used.
	private static final int NUMBER_OF_TEAMS = 6;

	// currentMatchID: used to keep track of the match that the user is
	// currently updating.
	protected static int currentMatchID;

	// currentTeamID: used to keep track of the team that the user is currently
	// updating.
	protected static int currentTeamID = -1;

	/**
	 * getRemoteConnection(): checks to see if con is initialised: if not, it is
	 * initialised.
	 * 
	 * @return con: the connection stored in the static variable above.
	 */

	public static Connection getRemoteConnection() {

		// Database access credentials.
		String url = "jdbc:mysql://rds-mysql-sixnations.c4jxximab4x4.eu-west-1.rds.amazonaws.com:3306";
		String username = "sixnations";
		String password = "password";

		// Checks to see if the connection has been initialised.
		if (con != null) {

			return con;

		}

		else {

			// Attempts to create a connection.
			try {

				// Inform the user that a connection is being created
				System.out.println("Connecting..");

				// This specifies which JDBC driver to make the connection with
				// to the DriverManager.
				Class.forName("com.mysql.jdbc.Driver");

				// Inform the user the driver has been loaded.
				System.out.println("Class loaded.");

				// Create the connection + inform the user of its creation.
				con = DriverManager.getConnection(url, username, password);
				System.out.println("Connected!\n");

				return con;

			} catch (SQLException | ClassNotFoundException e) {

				// Inform the user the connection was unsuccessful.
				System.out.println("Connection unsuccessful. Please try again.");
				e.printStackTrace();

				// Return a null value so connection cannot be used.
				return null;

			} // End of catch statement.

		} // End of else statement checking initialisation of the connection.

	} // End of getRemoteConnection() method.

	/**
	 * isDBInitialised(): checks against the very last value to be filled in the
	 * team table. It is filled sequentially - therefore if this value is
	 * filled, then all others must be (an SQLException occurring midway through
	 * the write would cause the writing to stop). It also checks the global
	 * Integer[][] fixtures in the Fixtures class. If fixtures exist, and the
	 * previous condition is true, the database is initialised.
	 * 
	 * @return
	 */
	protected static boolean isDBInitialised() {

		// Iterate through each fixture in the global fixtures array.
		for (Integer[] fixture : Fixtures.fixtures) {

			// For each fixture, iterate through each team.
			for (Integer team : fixture) {

				// If the team is equal to null, the fixtures have not been
				// initialised.
				if (team == null) {

					// This is the SQL for querying the database.
					String SQL = "SELECT * FROM sixnations.fixtures";

					try {

						// Create the statement, execute it and get the
						// ResultSet.
						Statement st = con.createStatement();
						st.executeQuery(SQL);

						ResultSet results = st.getResultSet();

						// If the ResultSet contains any values (i.e, the
						// fixtures table has been filled),
						// then the results are read from the database and
						// filled into the fixtures array.
						if (results.next()) {

							// Iterate through each match in the database.
							for (int match = 0; match < Fixtures.fixtures.length; match++) {

								// Iterate through each team in each match.
								for (int teamIndex = 0; teamIndex < Fixtures.fixtures[teamIndex].length; teamIndex++) {

									// Read team ID's from database and push
									// them to the fixtures array.
									Fixtures.fixtures[match][teamIndex] = readFromFixtures(match, teamIndex);

								} // End team for loop.

							} // End match for loop.

							/// selecting everything in the league table
							String allLeagueTable = "SELECT * FROM sixnations.league_table";
							Statement getLeague = con.createStatement();
							ResultSet emptyLeague = getLeague.executeQuery(allLeagueTable);
							// if the result of the selection from the league
							// table is empty....set all values to 0 except the
							// teamID's
							if (!emptyLeague.next()) {
								Statement updateLeague = con.createStatement();
								String createLeagueTable = "INSERT INTO sixnations.league_table VALUES (5,0,0,0,0,0,0,0), (4,0,0,0,0,0,0,0), (3,0,0,0,0,0,0,0), (2,0,0,0,0,0,0,0), (1,0,0,0,0,0,0,0), (0,0,0,0,0,0,0,0)";
								updateLeague.executeUpdate(createLeagueTable);

							} // end of if league Table empty

							// At this point, the database has been initialised.
							return true;

						} // End if statement checking ResultSet.

						// This code is executed if there are no results found
						// in the fixtures table.
						else {

							// The database has not been initialised.
							return false;

						}

					} catch (SQLException e) {

						e.printStackTrace();

					}

				} // End if statement.

			} // End team for loop.

		} // End match for loop.

		// If no null values are found, the database has been initialised.
		return true;

	} // End of isDBInitialised() method.

	/**
	 * readFromFixtures(): in the case of the database being initialised but not
	 * the fixtures array, this method reads through the fixtures table in the
	 * database and fills the array with the correct fixtures.
	 * 
	 * @param match:
	 *            matchID to check
	 * @param team:
	 *            teamID to check
	 * @return the teamID found at that location
	 */
	public static int readFromFixtures(int match, int team) {

		// This query will be generated based on the match and team being
		// selected.
		String SQL = "";

		// Result is initialised outside of the teamID range to easily identify
		// if it is not working.
		int result = -1;

		// If it is the home team (team 0), select team1_id.
		if (team == 0) {

			SQL = "SELECT team1_id FROM sixnations.fixtures WHERE match_id = " + (match + 1);

		}

		// Otherwise it is the away team (team 1).
		else if (team == 1) {

			SQL = "SELECT team2_id FROM sixnations.fixtures WHERE match_id = " + (match + 1);

		}

		else {

			System.out.println("Something went wrong - please try again!");

		}

		try {

			Statement st = con.createStatement();
			ResultSet results = st.executeQuery(SQL);

			while (results.next()) {

				// Get the teamID for the correct team.
				if (team == 0) {

					result = results.getInt("team1_id");

				}

				// Get the teamID for the correct team.
				else if (team == 1) {

					result = results.getInt("team2_id");

				}

			}

		} catch (SQLException e) {

			e.printStackTrace();

		}

		// The teamID of the home/away team playing in the specified match is
		// then returned.
		return result;

	}

	/**
	 * clearDB(): clears each table in the database in turn.
	 * this method is not open to the user!
	 */
	protected static void clearDB() {

		// These strings represent the queries needed to delete each table.
		String deleteTeams = "DELETE FROM sixnations.teams";
		String deleteFixtures = "DELETE FROM sixnations.fixtures";

		try {

			// Each update is executed in turn.
			Statement deleteStatement = con.createStatement();
			deleteStatement.executeUpdate(deleteTeams);
			deleteStatement.executeUpdate(deleteFixtures);

			// Inform the user.
			System.out.println("Database cleared!");

		} catch (SQLException e) {

			e.printStackTrace();

		}

	} // End of clearDB() method.

	/**
	 * writeToFixtures(): this method takes a match id, and two team ids, then
	 * writes them into the database.
	 * 
	 * @param matchID:
	 *            the match id
	 * @param team1ID:
	 *            the id of team 1
	 * @param team2ID:
	 *            the id of team 2
	 */
	protected static void writeToFixtures(int matchID, int team1ID, int team2ID) {

		try {

			String SQL = "INSERT INTO sixnations.fixtures VALUES (" + matchID + ", " + team1ID + ", " + team2ID + ")";
			Statement writeFixtures = con.createStatement();
			writeFixtures.executeUpdate(SQL);

		} catch (SQLException e) {

			e.printStackTrace();

		}

	} // End of writeToFixtures() method.

	protected static void writeToScores() {
		try {
			int round = 0;
			Scanner s = new Scanner(System.in);
			System.out.println("Which Round would you like to enter scores for: ");
			round = s.nextInt();

			while (round < 1 || round > 5) {

				System.out.println("Not a Valid Round - Select 1-5");
				round = s.nextInt();
			}

			int currentRound = round;
			// round*3 minus 3,4 and 5 always returns the 3 matches from the
			// previous round....all 3 must be filled in to begin entering
			// scores for next round
			String previousRoundMatches = "SELECT COUNT(*) FROM sixnations.scores WHERE match_id <=  " + (round * 3 - 3)
					+ " AND match_id >=  " + (round * 3 - 5);
			// check to see if all scores for previous round have been entered
			Statement roundScoresFinished = con.createStatement();
			ResultSet roundComplete = roundScoresFinished.executeQuery(previousRoundMatches);

			int scoresEntered = 0;
			if (currentRound > 1) {
				while (roundComplete.next()) {
					// if the result set does not have 6 results, all 6 scores
					// for the previous round have not been filled in
					scoresEntered = roundComplete.getInt(1);
				}
				if (scoresEntered != 6) {
					System.out.println("Scores for Round  " + (currentRound - 1)
							+ " must be complete before continuing to Round " + currentRound);
					writeToScores();

				}

			}

			// display fixtures in the round on screen
			readFromFixtures(currentRound);
			System.out.println("Please enter Match Number: ");
			int matchID = s.nextInt();
			// only accept match ID as a variable if it is displayed on screen -
			// while statement only runs if matchID is
			// within upper or lower round limits
			if (matchID < (currentRound * 3) - 2 || matchID > currentRound * 3) {

				System.out.println("Fixture is Not in This Round\nPlease enter Valid Match Number: ");
				matchID = s.nextInt();
			}

			currentMatchID = matchID;
			// getting team ID's from fixtures table
			String fixturesTable = "SELECT * FROM sixnations.fixtures WHERE match_id = " + currentMatchID;
			Statement getFix = con.createStatement();
			ResultSet fixes;
			fixes = getFix.executeQuery(fixturesTable);

			while (fixes.next()) {
				// assigns currentTeamID to teamID1 for score entry
				int teamID1 = fixes.getInt("team1_id");
				currentTeamID = teamID1;

				// checks to see if score for match has already been entered
				// after TeamID has been set
				isMatchEntered(currentMatchID);

				// if match scores have not been entered, begin entering scores
				// from user input
				System.out.println("Please enter the number of tries scored for " + getTeamName(currentTeamID));
				int tries1 = s.nextInt();
				System.out.println("Please enter the number of conversions scored for " + getTeamName(currentTeamID));
				int conversions1 = s.nextInt();
				while (conversions1 > tries1) {
					System.out.println(
							"Sorry you can not have more conversions than tries. Please enter the number of tries scored by "
									+ getTeamName(currentTeamID));
					tries1 = s.nextInt();
					System.out.println("Please enter the number of conversions scored for "
							+ getTeamName(fixes.getInt("team1_id")));
					conversions1 = s.nextInt();
				}
				System.out.println("Please enter the number of penalties scored for " + getTeamName(currentTeamID));
				int penalties1 = s.nextInt();
				int points1 = ((tries1 * 5) + (conversions1 * 2) + (penalties1 * 3));

				String SQL = "INSERT INTO sixnations.scores VALUES(" + currentMatchID + ", " + currentTeamID + ", "
						+ tries1 + ", " + conversions1 + ", " + penalties1 + ", " + points1 + ", null, null)";
				System.out.println("The total points for " + getTeamName(currentTeamID) + " comes to: " + points1);
				// System.out.println("Is this correct? Y//N");
				// String correct = s.nextLine();
				// if (correct.equalsIgnoreCase("Y")){
				Statement st = con.createStatement();
				st.executeUpdate(SQL);
				System.out.println("Scores for team " + getTeamName(currentTeamID) + " in Match " + currentMatchID
						+ " have been successfully added.");

				// away teamID defined

				int teamID2 = fixes.getInt("team2_id");
				currentTeamID = teamID2;

				System.out.println("Please enter the number of tries scored for " + getTeamName(currentTeamID));
				int tries2 = s.nextInt();
				System.out.println("Please enter the number of conversions scored for " + getTeamName(currentTeamID));
				int conversions2 = s.nextInt();
				while (conversions2 > tries2) {
					System.out.println(
							"Sorry you can not have more conversions than tries. Please enter the number of tries scored by "
									+ getTeamName(currentTeamID));
					tries2 = s.nextInt();
					System.out
							.println("Please enter the number of conversions scored for " + getTeamName(currentTeamID));
					conversions2 = s.nextInt();
				}
				System.out.println("Please enter the number of penalties scored for " + getTeamName(currentTeamID));
				int penalties2 = s.nextInt();
				int points2 = ((tries2 * 5) + (conversions2 * 2) + (penalties2 * 3));

				// 2nd team

				String squeuel = "INSERT INTO sixnations.scores VALUES(" + currentMatchID + ", " + currentTeamID + ", "
						+ tries2 + ", " + conversions2 + ", " + penalties2 + ", " + points2 + ", null, null)";
				System.out.println("The total points comes to: " + points2);
				// System.out.println("Is this correct? Y//N");
				// String correct = s.nextLine();
				// if (correct.equalsIgnoreCase("Y")){
				Statement statement = con.createStatement();
				statement.executeUpdate(squeuel);
				System.out.println("Scores for team " + getTeamName(teamID2) + " have been successfully added.");

				System.out.println("Final Score: " + getTeamName(fixes.getInt("team1_id")) + " " + points1 + " : "
						+ points2 + " " + getTeamName(fixes.getInt("team2_id")));

				// calculates wins and draws for match
				setWin(teamID1, points1, points2);
				setDraw(teamID1, points1, points2);
				setWin(teamID2, points2, points1);
				setDraw(teamID2, points2, points1);

				// passes team id, number of games played and points conceded to
				// method to be sent to league table

				// team1 info to be uploaded to the league table
				writeToLeagueTable(teamID1, gamesPlayed(teamID1), totalPointsScored(teamID1, points1),
						pointsConceded(teamID1, points2), pointsDifference(teamID1, points1, points2),
						totalTries(teamID1, tries1), calculateBonusPoints(teamID1, tries1, points1, points2),
						matchPoints(teamID1, points1, points2));
				// Team 2 info to be uploaded to the league table
				writeToLeagueTable(teamID2, gamesPlayed(teamID2), totalPointsScored(teamID2, points2),
						pointsConceded(teamID2, points1), pointsDifference(teamID2, points2, points1),
						totalTries(teamID2, tries2), calculateBonusPoints(teamID2, tries2, points2, points1),
						matchPoints(teamID2, points2, points1));

				System.out.println("League Table Updated...");

				Menu.displayMenu();// return to main menu after league table
									// updated

			} // catches any duplicate entries
		} catch (SQLException duplicateEntry) {
			// Gives user the option of how they want to handle the exception
			duplicateEntry.printStackTrace();
			removeDuplicateScores();
		}

	} // End of writeToScores() method.

	public static void removeDuplicateScores() {

		// methods for handling SQLIntegrity exception for duplicate entries
		try {

			// boolean startDelete = false;
			System.out.println("Original Scores Deleted......");
			String deleteDuplicates = "DELETE FROM sixnations.scores WHERE match_id = " + currentMatchID;

			// selects home teams ID for match
			String homeTeamScores = "SELECT * FROM sixnations.scores WHERE match_id = " + currentMatchID
					+ " AND team_id = " + currentTeamID;
			// selects away teams ID for match
			String awayTeamScores = "SELECT * FROM sixnations.scores WHERE match_id = " + currentMatchID
					+ " AND team_id != " + currentTeamID;

			Statement homeID = con.createStatement();
			Statement awayID = con.createStatement();

			int awayTeamID = -1;
			int homeTeamID = -1;
			ResultSet homeTeam = homeID.executeQuery(homeTeamScores);
			ResultSet awayTeam = awayID.executeQuery(awayTeamScores);
			while (homeTeam.next()) {

				homeTeamID = homeTeam.getInt("team_id");

			}
			while (awayTeam.next()) {
				awayTeamID = awayTeam.getInt("team_id");
			}
			// sends match ID to update the league table to effect the changes
			// for home team
			undoLeagueTableAfterDelete(currentMatchID, homeTeamID);
			// sends match ID to update the league table to effect the changes
			// for away team

			// waits until away match changes have taken effect on the league
			// table before deleting scores
			// startDelete = undoLeagueTableAfterDelete(currentMatchID,
			// awayTeamID);
			undoLeagueTableAfterDelete(currentMatchID, awayTeamID);
			System.out.print("Updating League Table...\n");
			// if (startDelete) {
			// deletes scores from the match that a duplicate entry was
			// detected
			Statement clearDuplicates = con.createStatement();
			clearDuplicates.executeUpdate(deleteDuplicates);
			Menu.displayMenu(); // returns user to main menu after deleting
								// duplicates if an up-to-date league table is
								// to be viewed
			// }
		} catch (SQLException s) {

			s.printStackTrace();
		}
	}

	public static int gamesPlayed(int teamID) {
		// returns number of games played for each team
		// using SQL query to determine what the games played are currently set
		// to
		// adding a 1 to this value, then returning it to be uploaded to the
		// league table

		String gamesPlayed = "SELECT games_played from sixnations.league_table WHERE team_id = " + teamID;
		int newGamesPlayed = 0;// value had to be initialised to zero...value is
								// reset from the result of the sql query
		try {
			Statement getGames = con.createStatement();
			ResultSet played = getGames.executeQuery(gamesPlayed);
			while (played.next()) {
				newGamesPlayed = played.getInt("games_played") + 1;
			}
		} catch (SQLException e) {
			e.printStackTrace();

			System.out.println("Problem Calculating Games Played");
		}
		return newGamesPlayed;
	}// end of method

	public static int pointsConceded(int teamID, int opponentPoints) {

		String concededPoints = "SELECT points_conceded from sixnations.league_table WHERE team_id = " + teamID;
		int newPointsConceded = 0;// value had to be initialised to zero...value
									// is reset from the result of the sql query
		try {
			Statement getPoints = con.createStatement();
			ResultSet points = getPoints.executeQuery(concededPoints);
			while (points.next()) {
				newPointsConceded = points.getInt("points_conceded") + opponentPoints;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return newPointsConceded;

	}

	public static int pointsDifference(int teamID, int pointsFor, int pointsAgainst) {

		int newPointsDifference = 0;
		String pointsDifference = "SELECT points_difference from sixnations.league_table WHERE team_id = " + teamID;
		try {
			Statement getPointsDifference = con.createStatement();
			ResultSet pointsDiff = getPointsDifference.executeQuery(pointsDifference);
			while (pointsDiff.next()) {
				newPointsDifference = pointsDiff.getInt("points_difference") + (pointsFor - pointsAgainst);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return newPointsDifference;

	}

	public static int totalPointsScored(int teamID, int pointsScored) {
		int newTotalPointsScored = 0;
		String totalPoints = "SELECT points_scored from sixnations.league_table WHERE team_id = " + teamID;
		try {
			Statement getTotalPointsScored = con.createStatement();
			ResultSet totPointsScored = getTotalPointsScored.executeQuery(totalPoints);
			while (totPointsScored.next()) {
				newTotalPointsScored = totPointsScored.getInt("points_scored") + pointsScored;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return newTotalPointsScored;
	}

	public static int matchPoints(int teamID, int pointsScored, int pointsConceded) {

		int newTotalPoints = 0;
		String leaguePoints = "SELECT total_points from sixnations.league_table WHERE team_id = " + teamID;

		try {
			Statement getLeaguePoints = con.createStatement();
			ResultSet totalPoints = getLeaguePoints.executeQuery(leaguePoints);
			while (totalPoints.next()) {
				// return total points + 4 for a team1 win
				if (pointsScored > pointsConceded) {
					newTotalPoints = totalPoints.getInt("total_points") + 4;
					// return total points + 4 for a team2 win
				} else if (pointsScored == pointsConceded) {
					newTotalPoints = totalPoints.getInt("total_points") + 2;
					// keep the same total for a loss
				} else {
					newTotalPoints = totalPoints.getInt("total_points");
				}

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return newTotalPoints;

	}

	public static void setWin(int teamID, int pointsScored, int pointsConceded) {

		boolean resultBool = false;

		if (pointsScored > pointsConceded) {
			resultBool = true;

		} else {
			resultBool = false;
		}

		try {
			System.out.println("running win method");
			String setWin = "UPDATE sixnations.scores SET win = " + resultBool + " WHERE team_id = " + teamID
					+ " AND match_id = " + currentMatchID;
			Statement win = con.createStatement();
			win.executeUpdate(setWin);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Problem setting win boolean");
		}

	}

	public static void setDraw(int teamID, int pointsScored, int pointsConceded) {

		boolean resultBool = false;

		if (pointsScored == pointsConceded) {
			resultBool = true;

		} else {
			resultBool = false;
		}
		System.out.println("running draw method");
		try {
			String setDraw = "UPDATE sixnations.scores SET draw = " + resultBool + " WHERE team_id = " + teamID
					+ " AND match_id = " + currentMatchID;
			Statement draw = con.createStatement();
			draw.executeUpdate(setDraw);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Problem setting darw boolean");
		}

	}

	public static int totalTries(int teamID, int triesScored) {

		int newTotalTries = 0;

		String totalTries = "SELECT tries_scored from sixnations.league_table WHERE team_id = " + teamID;
		try {
			Statement getTotalTries = con.createStatement();
			ResultSet totTriesScored = getTotalTries.executeQuery(totalTries);
			while (totTriesScored.next()) {
				newTotalTries = totTriesScored.getInt("tries_scored") + triesScored;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return newTotalTries;

	}

	public static int calculateBonusPoints(int teamID, int tries, int pointsScored, int pointsConceded) {

		int pointsDiff = pointsScored - pointsConceded; // used to calculate
														// margin or win or loss
		int bonusPointsGained = 0; // counts bonus points as games scores are
									// entered
		int totalBonusPoints = 0; // adds the current bonus points in the table
									// to the bonusPointsGained as scores are
									// entered

		String bonusPoints = "SELECT bonus_points from sixnations.league_table WHERE team_id = " + teamID;
		try {
			Statement getBonusPoints = con.createStatement();
			ResultSet totBonusPoints = getBonusPoints.executeQuery(bonusPoints);
			while (totBonusPoints.next()) {
				// if 4 or more tries are scored and the team has won, add 1 to
				// bonus points
				if (pointsScored > pointsConceded && tries >= 4) {
					bonusPointsGained++;
				}

				// if team loses match by a margin of 7 or less and scores more
				// than 4 tries...+2 bonus point
				if (pointsConceded > pointsScored && pointsDiff >= -7 && tries >= 4) {
					bonusPointsGained = bonusPointsGained + 2;

					// if team loses match by a margin of 7 or less or scores
					// more than 4 tries...+1 bonus point
				} else if (pointsConceded > pointsScored && pointsDiff >= -7
						|| pointsConceded > pointsScored && tries >= 4) {
					bonusPointsGained++;
				}
				// if the match is a draw and the team scores more than 4
				// tries...+1 bonus point
				if (pointsDiff == 0 && tries >= 4) {
					bonusPointsGained++;
				}

				// calculate bonus points for the match and add that to the
				// bonus points earned from previous games
				totalBonusPoints = totBonusPoints.getInt("bonus_points") + bonusPointsGained;

				//////// add these bonus points to the total League points for
				//////// the team
				addBonusPointsToLeague(teamID, bonusPointsGained);

			}
			// end of while
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return totalBonusPoints;

	}

	public static void addBonusPointsToLeague(int teamID, int bonusPoints) {

		int newTotalPoints = 0;
		String leaguePoints = "SELECT total_points from sixnations.league_table WHERE team_id = " + teamID;

		try {
			// getting current league points
			Statement getLeaguePoints = con.createStatement();
			ResultSet totalPoints = getLeaguePoints.executeQuery(leaguePoints);

			while (totalPoints.next()) {
				newTotalPoints = totalPoints.getInt("total_points") + bonusPoints;
			}

			// updating league points to included bonus points
			String leagueWithBonus = "UPDATE sixnations.league_table SET total_points = " + newTotalPoints
					+ " WHERE team_id = " + teamID;
			Statement leagueAndBonus = con.createStatement();
			leagueAndBonus.executeUpdate(leagueWithBonus);

		} catch (SQLException e) {

			e.printStackTrace();

		}
	}

	public static void undoLeagueTableAfterDelete(int matchID, int teamID) {
		// method to undo changed to the league table after a result is deleted
		// because of a duplicate entry

		// System.out.println("got this far....start of getting current
		// Values");
		// selects home teams scores
		String homeTeamScores = "SELECT * FROM sixnations.scores WHERE match_id = " + matchID + " AND team_id = "
				+ teamID;
		// opponent points scored needed to calculate bonus points earned from
		// the match
		String opponentPoints = "SELECT points FROM sixnations.scores WHERE match_id = " + matchID + " AND team_id != "
				+ teamID;
		// calculating the teams scores in the league table
		String leagueTableBeforeDelete = "SELECT * FROM sixnations.league_table WHERE team_id = " + teamID;

		try {
			Statement leagueTable = con.createStatement();
			Statement scoresTable = con.createStatement();
			Statement opponentPoint = con.createStatement();
			ResultSet currentLeagueTable = leagueTable.executeQuery(leagueTableBeforeDelete);
			ResultSet scoresUndo = scoresTable.executeQuery(homeTeamScores);
			ResultSet opponentScored = opponentPoint.executeQuery(opponentPoints);

			// getting the current values entered into the scores table for the
			// team in the match that is being deleted
			int matchTries = 0;
			int matchBonusPoints = 0;
			int matchPointsScored = 0;
			int matchLeaguePoints = 0;
			while (scoresUndo.next()) {

				matchTries = scoresUndo.getInt("tries");
				matchPointsScored = scoresUndo.getInt("points");
			}

			// System.out.println("matchTries: " + matchTries);
			// System.out.println("matchConversions: " + matchConversions);
			// System.out.println("matchPenalties: " + matchPenalties);

			// getting the number of points the opponent scored in this match to
			// obtain number of bonus points earned
			int opponentMatchPoints = 0;
			while (opponentScored.next()) {

				opponentMatchPoints = opponentScored.getInt("points");
			}
			// calculates total points scored in the match

			//// calculating bonus points earned in match
			// if a team wins and scores 4 of more tries, 1 bonus point
			if (matchPointsScored > opponentMatchPoints && matchTries >= 4) {
				matchBonusPoints++;
			}

			// if team loses match by a margin of 7 or less and scores more
			// than 4 tries...+2 bonus point
			if (opponentMatchPoints > matchPointsScored && (matchPointsScored - opponentMatchPoints) >= -7
					&& matchTries >= 4) {
				matchBonusPoints = matchBonusPoints + 2;

				// if team loses match by a margin of 7 or less or scores
				// more than 4 tries...+1 bonus point
			} else if (opponentMatchPoints > matchPointsScored && (matchPointsScored - opponentMatchPoints) >= -7
					|| opponentMatchPoints > matchPointsScored && matchTries >= 4) {
				matchBonusPoints++;
			}
			// if the match is a draw and the team scores more than 4
			// tries...+1 bonus point
			if ((matchPointsScored - opponentMatchPoints) == 0 && matchTries >= 4) {
				matchBonusPoints++;
			}

			// calculates the league points won in this match
			if (matchPointsScored > opponentMatchPoints) {
				matchLeaguePoints = 4;
			} else if (matchPointsScored == opponentMatchPoints) {
				matchLeaguePoints = 2;
			}

			// getting the current league and bonus points earned to calculate
			// whether a grandslam has been won to deduct the 3 bonus points
			// reset games played by 1
			int newGamesPlayed = 0;
			int newPointsScored = 0;
			int newPointsConceded = 0;
			int newPointsDifference = 0;
			int newTriesScored = 0;
			int newBonusPoints = 0;
			int newTotalPoints = 0;
			while (currentLeagueTable.next()) {

				// reset games played by 1
				newGamesPlayed = (currentLeagueTable.getInt("games_played") - 1);
				/// deducts the points scored in the match from the current
				/// tournament points scored
				newPointsScored = (currentLeagueTable.getInt("points_scored") - matchPointsScored);
				newPointsConceded = (currentLeagueTable.getInt("points_conceded") - opponentMatchPoints);
				// caclulate new league points difference
				newPointsDifference = (currentLeagueTable.getInt("points_difference")
						- (matchPointsScored - opponentMatchPoints));
				newTriesScored = currentLeagueTable.getInt("tries_scored") - matchTries;

				// if total points before delete - bonus points before delete
				// =20, then the grandslam has been won - 3 bonus and total
				// points need to be deducted;
				if ((currentLeagueTable.getInt("total_points") - currentLeagueTable.getInt("bonus_points")) == 20) {
					matchBonusPoints = matchBonusPoints + 3;
				}

				// calculating how many bonus points were earned in the match or
				// by
				// a grandslam win
				newBonusPoints = currentLeagueTable.getInt("bonus_points") - matchBonusPoints;

				// calculating if the game was won, deducting the result points,
				// plus the bonus points earned in the game

				newTotalPoints = (currentLeagueTable.getInt("total_points") - (matchLeaguePoints + matchBonusPoints));
			}

			String updateLeagueTable = "UPDATE sixnations.league_table SET games_played =  " + newGamesPlayed
					+ ", points_scored = " + newPointsScored + ", points_conceded = " + newPointsConceded
					+ ", points_difference = " + newPointsDifference + ", tries_scored = " + newTriesScored
					+ ", bonus_points = " + newBonusPoints + ", total_points = " + newTotalPoints + " WHERE team_id = "
					+ teamID;
			Statement update = con.createStatement();
			update.executeUpdate(updateLeagueTable);

		} catch (

		SQLException e) {

			e.printStackTrace();
			System.out.println("Houston, somethings up");
		}

	}

	public static void isMatchEntered(int matchID) {
		try {
			Scanner entry = new Scanner(System.in);
			int decision;
			String isMatchComplete = "SELECT COUNT(*) FROM sixnations.scores WHERE match_id = " + matchID;
			Statement completeMatch = con.createStatement();
			ResultSet matchEntered = completeMatch.executeQuery(isMatchComplete);

			while (matchEntered.next()) {

				// if one or more results have been entered for the match
				if (matchEntered.getInt(1) > 0) {

					System.out.println(
							"There are already Scores Entered for this match.\nPress 1 to Delete These Scores and Re Enter new Scores\nPress 2 to return to the Main Menu");
					decision = entry.nextInt();

					if (decision == 1) {
						removeDuplicateScores();
					} else if (decision == 2) {

						Menu.displayMenu();
					}

				}

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	public static void generateTable() {

		// Create the league table
		// might have to add columns for total points etc...
		try {

			String getLeagueTable = "SELECT * FROM sixnations.league_table ORDER BY (total_points) DESC";
			Statement showLeagueTable = con.createStatement();
			showLeagueTable.executeQuery(getLeagueTable);
			ResultSet rsLeague = showLeagueTable.getResultSet();
			ResultSetMetaData rsmd = rsLeague.getMetaData();
			System.out.println("\n\t\t\t Six Nations League Table\n ");

			for (int i = 1; i <= rsmd.getColumnCount(); i++) {

				System.out.print(rsmd.getColumnName(i) + "\t|\t");

			}
			System.out.println(
					"\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

			while (rsLeague.next()) {
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					if (i == 1) {
						if ((rsLeague.getInt("team_id") != 4)) {
							System.out.print(getTeamName(rsLeague.getInt("team_id")) + "\t|\t");
						} else {
							System.out.print(getTeamName(rsLeague.getInt("team_id")) + "|\t");
						}
					} else if (i != 1) {

						if (i == 5) {
							System.out.print("\t\t" + rsLeague.getInt(i) + "\t|\t");

						} else {

							System.out.print("\t" + rsLeague.getInt(i) + "\t|\t");
						}
					}
				}
				System.out.println();
			}
			System.out.println("\n");
		} catch (SQLException sqerror) {

			sqerror.printStackTrace();
		}

	}

	public static void writeToLeagueTable(int team_id, int games_played, int points_scored, int points_conceded,
			int points_difference, int tries_scored, int bonus_points, int total_points) {

		try {

			Statement update = con.createStatement();
			String leagueTable = "UPDATE sixnations.league_table SET games_played =" + games_played
					+ ", points_scored =" + points_scored + ", points_conceded =" + points_conceded
					+ ", points_difference =" + points_difference + ", tries_scored =" + tries_scored
					+ ", bonus_points = " + bonus_points + ", total_points =" + total_points + " WHERE team_id ="
					+ team_id;
			update.executeUpdate(leagueTable);

			// checks to see if grandslam has been won after updating the league
			// table
			checkGrandSlam(team_id);

		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	public static void checkGrandSlam(int team_id) {

		// SQL Query to ensure all 5 games have been played before grandslam can
		// be won
		String grandSlam = "SELECT * FROM sixnations.league_table WHERE team_id = " + team_id + " AND games_played = 5";

		try {
			Statement grande = con.createStatement();
			ResultSet winner = grande.executeQuery(grandSlam);

			if (winner.next()) {

				int totalWithBonus = winner.getInt("total_points");
				int totalBonusPoints = winner.getInt("bonus_points");

				// grand slam calculated as when TotalPoints - Bonus Points =
				// 20, then the team has won all 5 games
				if (totalWithBonus - totalBonusPoints == 20) {

					System.out.println(getTeamName(team_id) + " has won the Grand Slam!");

					String updateSlamTotal = "UPDATE sixnations.league_table SET total_points = " + (totalWithBonus + 3)
							+ " WHERE team_id = " + team_id;
					String updateSlamBonus = "UPDATE sixnations.league_table SET bonus_points = "
							+ (totalBonusPoints + 3) + " WHERE team_id = " + team_id;

					Statement updateTotal = con.createStatement();
					updateTotal.executeUpdate(updateSlamTotal);
					updateTotal.executeUpdate(updateSlamBonus);

					System.out.println("Bonus Points For Grand Slam Win added to Totals....");

				}
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	/**
	 * writeToTeams(): this method fills a HashMap with the name and id of each
	 * team, then writes it into the database.
	 */
	protected static void writeToTeams() {

		// HashMap is populated using fillTeams() method.
		HashMap<Integer, String> teams = Teams.fillTeams();

		// Iterate through each team, and store their id and name
		for (int teamNumber = 0; teamNumber < NUMBER_OF_TEAMS; teamNumber++) {

			Integer teamID = teamNumber;
			String teamName = teams.get(teamNumber);

			try {

				// Use the stored values to write each team into the database.
				String SQL = "INSERT INTO sixnations.teams VALUES (" + teamID + ", '" + teamName + "')";
				Statement st = con.createStatement();
				st.executeUpdate(SQL);

			} catch (SQLException e) {

				e.printStackTrace();

			}

		} // End for loop.

	} // End of writeToTeams() method.

	protected static void readFromFixtures(int roundChoice) {

		// These ints keep track of the first and last match of each round.
		// (i.e for round 1, the lowMatch will be 1 and the highMatch will be 3.
		int lowMatch = 0;
		int highMatch = 0;

		if (roundChoice == 1) {

			lowMatch = 1;
			highMatch = 3;

		}

		// This sets the low and high matches based on the round that has been
		// selected.
		else {

			lowMatch = (3 * (roundChoice - 1) + 1);
			highMatch = (3 * (roundChoice - 1) + 3);

		}

		// Prints a round header
		System.out.println();
		System.out.println("\tRound " + roundChoice);
		System.out.println();

		try {

			// This SQL query selects matches from the scores table where the
			// match id is between lowMatch and highMatch.
			// i.e for round 1, this will return matches 1-3.
			Statement st = con.createStatement();
			String SQL = "SELECT * FROM sixnations.fixtures WHERE match_id BETWEEN " + lowMatch + " and " + highMatch;

			st.executeQuery(SQL);
			ResultSet results = st.getResultSet();

			// Formatting of the results.
			while (results.next()) {
				// getTeamName converts TeamID to a name
				System.out.println("Match Number: " + results.getInt("match_id") + ".\t"
						+ getTeamName(results.getInt("team1_id")) + " vs. " + getTeamName(results.getInt("team2_id")));

			}

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	protected static void readFromScores(int roundChoice) {

		// These ints keep track of the first and last match of each round.
		// (i.e for round 1, the lowMatch will be 1 and the highMatch will be 3.
		int lowMatch = 0;
		int highMatch = 0;

		if (roundChoice == 1) {

			lowMatch = 1;
			highMatch = 3;

		}

		// This sets the low and high matches based on the round that has been
		// selected.
		else {

			lowMatch = (3 * (roundChoice - 1) + 1);
			highMatch = (3 * (roundChoice - 1) + 3);

		}

		// Prints a round header
		System.out.println();
		System.out.println("\t---------");
		System.out.println("\t Round " + (roundChoice));
		System.out.println("\t---------");
		System.out.println();

		try {

			// This SQL query selects matches from the scores table where the
			// match id is between lowMatch and highMatch.
			// i.e for round 1, this will return matches 1-3.
			Statement st = con.createStatement();
			String SQL = "SELECT * FROM sixnations.scores WHERE match_id BETWEEN " + lowMatch + " and " + highMatch;

			st.executeQuery(SQL);
			ResultSet results = st.getResultSet();

			// Formatting of the results.
			while (results.next()) {

				String team1 = getTeamName(results.getInt("team_id"));
				int points1 = results.getInt("points");

				results.next();

				String team2 = getTeamName(results.getInt("team_id"));
				int points2 = results.getInt("points");

				String preSpacing;
				String spacing = " ";

				if (team1.length() == 8) {

					preSpacing = "";

				}

				else if (team1.length() == 7) {

					preSpacing = " ";

				}

				else if (team1.length() == 6) {

					preSpacing = "  ";

				}

				else if (team1.length() == 5) {

					preSpacing = "   ";

				}

				else {

					preSpacing = "";

				}

				System.out.println(preSpacing + team1 + spacing + points1 + " - " + points2 + " " + team2);

			}

			System.out.println();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	} // End of readFromScores() method.

	/**
	 * uploadRoundScoresWindows(): handles the SQL query generation relating to
	 * uploading round scores when on Windows systems.
	 */
	protected static void uploadRoundScoresWindows(int round) {

		try {
			
			String SQL = "SELECT sixnations.games_played FROM league_table";
			Statement st = con.createStatement();
			st.executeQuery(SQL);
			
			ResultSet rs = st.getResultSet();
			
			while (rs.next())
			{
				
				if (rs.getInt("games_played") == 5)
				{
					
					System.out.println("All games already played.");
					return;
					
				}
				
			}
			

			// Find the filepath of the project on the user's system.
			String userFilepath = System.getProperty("user.dir");

			// Replace any forward slashes in the filepath with back slashes, so
			// SQL can interpret it correctly.
			// There needs to be 4 back slashes for SQL to read it as one.
			userFilepath = userFilepath.replace("\\", "\\\\");

			// roundString: this will be set based on the user's selection of
			// round.
			String roundString = "";

			// Generate the correct file based on user selection.
			switch (round) {

			case 1:
				roundString = "\\round1.txt";
				break;

			case 2:
				roundString = "\\round2.txt";
				break;

			case 3:
				roundString = "\\round3.txt";
				break;

			case 4:
				roundString = "\\round4.txt";
				break;

			case 5:
				roundString = "\\round5.txt";
				break;

			// In the event of an invalid round, the uploadRoundScores() method
			// is called again.
			default:
				System.out.println("Invalid round. Please try again");
				Menu.uploadRoundScores();
				break;

			} // End switch statement.

			// Generate a SQL query from the variables set up previously, and
			// execute it.
			String roundQuery = "LOAD DATA LOCAL INFILE \'" + userFilepath + "\\" + roundString
					+ "\' INTO TABLE sixnations.scores";
			Statement roundStatement = con.createStatement();
			roundStatement.execute(roundQuery);
			System.out.println("Round loaded!\n");

			// Pull data from scores
			updateTable(round);

		} catch (Exception e) {

			e.printStackTrace();

			// Stop the menu from looping.
			Menu.menuRunning = false;

		}

	} // End of uploadRoundScoresWindows() method.

	/**
	 * uploadRoundScoresWindows(): handles the SQL query generation relating to
	 * uploading round scores when on Mac systems.
	 */
	protected static void uploadRoundScoresMac(int round) {

		try {
			
			String SQL = "SELECT games_played FROM sixnations.league_table";
			Statement st = con.createStatement();
			st.executeQuery(SQL);
			
			ResultSet rs = st.getResultSet();
			
			while (rs.next())
			{
				
				if (rs.getInt("games_played") == 5)
				{
					
					System.out.println("All games already played.");
					return;
					
				}
				
			}

			// Find the filepath of the project on the user's system.
			String userFilepath = System.getProperty("user.dir");

			// roundString: this will be set based on the user's selection of
			// round.
			String roundString = "";

			// Generate the correct file based on user selection.
			switch (round) {

			case 1:
				roundString = "round1.txt";
				break;

			case 2:
				roundString = "round2.txt";
				break;

			case 3:
				roundString = "round3.txt";
				break;

			case 4:
				roundString = "round4.txt";
				break;

			case 5:
				roundString = "round5.txt";
				break;

			// In the event of an invalid round, the uploadRoundScores() method
			// is called again.
			default:
				System.out.println("Invalid round. Please try again");
				Menu.uploadRoundScores();
				break;

			} // End switch statement.

			// Generate a SQL query from the variables set up previously, and
			// execute it.
			String roundQuery = "LOAD DATA LOCAL INFILE \'" + userFilepath + "/" + roundString
					+ "\' INTO TABLE sixnations.scores";
			Statement roundStatement = con.createStatement();
			roundStatement.execute(roundQuery);
			System.out.println("Round loaded!\n");

			updateTable(round);

		} catch (Exception e) {

			e.printStackTrace();

			// Stop the menu from looping.
			Menu.menuRunning = false;

		}

	} // End of uploadRoundScoresMac() method.

	private static void updateTable(int round) {

		int lowMatch;
		int highMatch;

		if (round == 1) {

			lowMatch = 1;
			highMatch = 3;

		}

		// This sets the low and high matches based on the round that has been
		// selected.
		else {

			lowMatch = (3 * (round - 1) + 1);
			highMatch = (3 * (round - 1) + 3);

		}

		String SQL = "SELECT team_id, points, tries FROM sixnations.scores WHERE match_id BETWEEN " + lowMatch + " AND "
				+ highMatch;

		try {

			Statement st = con.createStatement();
			st.executeQuery(SQL);

			ResultSet rs = st.getResultSet();

			while (rs.next()) {

				int teamID1 = rs.getInt("team_id");
				int points1 = rs.getInt("points");
				int tries1 = rs.getInt("tries");

				rs.next();

				int teamID2 = rs.getInt("team_id");
				int points2 = rs.getInt("points");
				int tries2 = rs.getInt("tries");

				writeToLeagueTable(teamID1, gamesPlayed(teamID1), totalPointsScored(teamID1, points1),
						pointsConceded(teamID1, points2), pointsDifference(teamID1, points1, points2),
						totalTries(teamID1, tries1), calculateBonusPoints(teamID1, tries1, points1, points2),
						matchPoints(teamID1, points1, points2));

				writeToLeagueTable(teamID2, gamesPlayed(teamID2), totalPointsScored(teamID2, points2),
						pointsConceded(teamID2, points1), pointsDifference(teamID2, points2, points1),
						totalTries(teamID2, tries2), calculateBonusPoints(teamID2, tries2, points2, points1),
						matchPoints(teamID2, points2, points1));

			}

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public static void searchDB() {

		try {

			boolean searchMenuInUse = true;
			int searchChoice;
			do {

				System.out.println("\tSearch Menu");
				System.out.println("---------------------------");
				System.out.println("1. Team Win percentage");
				System.out.println("2. Team Loss percentage");
				System.out.println("3. Most tries");
				System.out.println("4. Most Conversions");
				System.out.println("5. Most penalties points");
				System.out.println("6. Most Points");
				System.out.println("7. Least tries");
				System.out.println("8. Least conversions");
				System.out.println("9. Least Penalties");
				System.out.println("10. Least points");
				System.out.println("11. Return to main menu");
				System.out.println();
				System.out.println("---------------------------");

				System.out.println();
				System.out.println("please select an option.");

				Scanner s = new Scanner(System.in);

				searchChoice = s.nextInt();

				try {
					if (searchChoice > 0 && searchChoice <= 12) {
						switch (searchChoice) {
						case 1:
							printTeamDetails();
							int choice = s.nextInt();
							System.out.println(winPercentage(choice));
							break;
						case 2:
							printTeamDetails();
							int choice2 = s.nextInt();
							System.out.println(lossPercentage(choice2));
							break;
						case 3:
							System.out.println(mostTries());
							break;
						case 4:
							System.out.println(mostConversions());
							break;
						case 5:
							System.out.println(mostPenalties());
							break;
						case 6:
							System.out.println(mostPoints());
							break;
						case 7:
							System.out.println(leastTries());
						case 8:
							System.out.println(leastConversions());
							break;
						case 9:
							System.out.println(leastPenalties());
							break;
						case 10:
							System.out.println(leastPoints());
							break;
						case 11:
							// kills the search menu and closes the scanner,
							// then returns user to the main menu.
							searchMenuInUse = false;
							// s.close(); //closing scanner causes exception as
							// it is used by displayMenu
							Menu.displayMenu();
							break;
						default:
							break;
						}
					} else {
						s.close();
						throw new MenuChoiceException();
					}
				} catch (MenuChoiceException | InputMismatchException ex) {
					ex.printStackTrace();
				}
			} while (searchMenuInUse);

		} catch (

		Exception e) {
			e.printStackTrace();
		}

	} // End of searchDB() method.

	public static String printTeamDetails() {
	
		String teamDetails ="1. England\n2. France\n3. Ireland\n4. Italy\n5. Scotland\n6. Wales\nPlease select the team you would like to search for:";
		return teamDetails;
		
	} // End of printTeamDetails() method.

	/**
	 * runs two queries that will determine the number of games played by a team
	 * and the number of games they have won. will then calculate the percentage
	 * of the games that have been won.
	 * 
	 * Niall - have edited this search a bit since the 'winner' column has been
	 * removed should still function as intended :) readded win and draw colummn
	 * for this calculation
	 * 
	 * @param teamID
	 *            the ID number of the team to be search for.
	 * @throws SQLException
	 */
	public static String winPercentage(int teamID) throws SQLException {
		teamID = teamID - 1; // minus 1 as team_id's range from 0-5

		double gamesWon=-1;
		double gamesPlayed=0;
		String result;

		// query to count the number of wins
		String query = "SELECT SUM(win) FROM sixnations.scores WHERE team_id = " + teamID + " AND win = true";
		String games = "SELECT games_played from sixnations.league_table WHERE team_id  = " + teamID;

		Statement st = con.createStatement();
		Statement sta = con.createStatement();
		ResultSet rs = st.executeQuery(query);
		ResultSet rsGames = sta.executeQuery(games);

		rs.next();
		gamesWon = rs.getInt(1);
		rsGames.next();
		gamesPlayed = rsGames.getInt("games_played");

		// calculates and prints out the percentages of wins for the team
		double winPercentage = (gamesWon / gamesPlayed) * 100;

		// check to make sure a win percentage can be calculated...if not,
		// prints to screen to prompt to enter scores
		if (Double.isNaN(winPercentage)) {

			result = (getTeamName(teamID) + " have not won any games yet.");

		} else {
			result = (getTeamName(teamID) + " has won " + winPercentage + "% of " + gamesPlayed + " game(s)");
		}
		return result;

	}
	
	/**
	 * runs two queries that will determine the amount of games a team has
	 * played and the number of games it has lost then calculates the percentage
	 * of games lost
	 * 
	 * @param teamID
	 *            the id of the team that will be search for
	 * @throws SQLException
	 */
	public static String lossPercentage(int teamID) throws SQLException {
		teamID = teamID - 1; // minus 1 as team_id's range from 0-5
		double gamesWon=0;
		double gamesDrawn=0;
		double gamesLost=0;
		double gamesPlayed=0;
		String result;
	
		// first query determines the number of games the team have won.
		String query = " SELECT SUM(win), SUM(draw) FROM sixnations.scores WHERE team_id = " + teamID
				+ " AND win = true AND DRAW = true";
	
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
	
		String games = "SELECT games_played from sixnations.league_table WHERE team_id  = " + teamID;
		Statement sta = con.createStatement();
		ResultSet rsGames = sta.executeQuery(games);
	
		rs.next();
		gamesWon = rs.getInt(1);
		gamesDrawn = rs.getInt(2);
	
		rsGames.next();
		gamesPlayed = rsGames.getInt("games_played");
	
		// gamesPlayed - gamesWon = gamesLost or drawn
	
		if (Double.isNaN(gamesWon)) {
	
			gamesWon = 0;
		}
		if (Double.isNaN(gamesDrawn)) {
	
			gamesDrawn = 0;
		}
	
		gamesLost = gamesPlayed - (gamesWon + gamesDrawn);
	
		// calculates and prints out the percentages of loses for the team
		double lossPercentage = (gamesLost / gamesPlayed) * 100;
	
		System.out.println(lossPercentage + "test");
		if (Double.isNaN(lossPercentage)) {
	
			result = (getTeamName(teamID) + " have not lost any games yet.");
	
		} else {
	
			result = (getTeamName(teamID) + " has lost " + lossPercentage + "% of " + gamesPlayed + " game(s)");
		}
		return result;
	}
	
	/**
	 * runs a SQL query to determine which team has scored the most tries
	 * 
	 * @return
	 * 
	 * @throws SQLException
	 */
	public static String mostTries() throws SQLException {
		String query = "SELECT team_id, SUM(tries) from sixnations.scores GROUP BY (team_id) ORDER BY (SUM(tries)) DESC LIMIT 1";
	
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
	
		String result = "";
	
		rs.next();
		int teamID = rs.getInt(1);
		int mostTries = rs.getInt(2);
	
		if (mostTries < 1) {
	
			result = "No tries have been scored yet";
	
		} else {
	
			result = (getTeamName(teamID) + " has the most tries with: " + mostTries);
	
		}
		return result;
	}
	
	/**
	 * runs a SQL query to determine which team has scored the most conversions
	 * 
	 * @throws SQLException
	 */
	
	public static String mostConversions() throws SQLException {
		String query = "SELECT team_id, SUM(conversions) from sixnations.scores GROUP BY (team_id) ORDER BY (SUM(tries)) DESC LIMIT 1";
		String result;
	
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
	
		rs.next();
		int teamID = rs.getInt(1);
		int mostConversions = rs.getInt(2);
	
		if (mostConversions < 1) {
	
			result = "No conversions have been scored yet";
		} else {
			result = (getTeamName(teamID) + " has the most conversions with: " + mostConversions);
		}
		return result;
	
	}
	
	/**
	 * runs a SQL query to determine which team has scored the most penalties
	 * 
	 * @throws SQLException
	 */
	public static String mostPenalties() throws SQLException {
		String query = "SELECT team_id, SUM(penalties) from sixnations.scores GROUP BY (team_id) ORDER BY (SUM(penalties)) DESC LIMIT 1";
	
		String result;
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
	
		rs.next();
		int teamID = rs.getInt(1);
		int mostPenalties = rs.getInt(2);
	
		if (mostPenalties < 1) {
	
			result = "No penalties have been scored yet";
		} else {
			result = (getTeamName(teamID) + " has the most penalties with: " + mostPenalties);
		}
		return result;
	
	}
	
	/**
	 * runs a SQL query to determine which teams has gained the most points
	 * 
	 * @throws SQLException
	 */
	public static String mostPoints() throws SQLException {
		String query = "SELECT team_id, MAX(points_scored) from sixnations.league_table GROUP BY(team_id) ORDER BY(MAX(points_scored)) DESC LIMIT 1";
	
		String result;
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
	
		rs.next();
		int teamID = rs.getInt(1);
		int mostPoints = rs.getInt(2);
	
		if (mostPoints < 1) {
	
			result = "No points have been scored yet";
		} else {
			result = (getTeamName(teamID) + " has the most points scored with: " + mostPoints);
		}
	
		return result;
	
	}
	
	/**
	 * runs a SQL query to determine which team has scored the least tries
	 * 
	 * @throws SQLException
	 */
	public static String leastTries() throws SQLException {
		String query = "SELECT team_id, SUM(tries) from sixnations.scores GROUP BY (team_id) ORDER BY (SUM(tries)) ASC LIMIT 1";
	
		String result;
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
	
		rs.next();
		int teamID = rs.getInt(1);
		int minTries = rs.getInt(2);
	
		if (minTries < 0) {
	
			result = "No tries have been scored yet";
		} else {
			result = (getTeamName(teamID) + " has the least tries with: " + minTries);
		}
		return result;
	}
	
	/**
	 * runs a SQL query to determine which team has scored the least conversions
	 * 
	 * @throws SQLException
	 */
	public static String leastConversions() throws SQLException {
		String query = "SELECT team_id, SUM(conversions) from sixnations.scores GROUP BY (team_id) ORDER BY (SUM(tries)) ASC LIMIT 1";
	
		String result;
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
	
		rs.next();
		int teamID = rs.getInt(1);
		int minConversions = rs.getInt(2);
	
		if (minConversions < 0) {
	
			result = "No conversions have been scored yet";
	
		} else {
			result = (getTeamName(teamID) + " has the least conversions with: " + minConversions);
		}
	
		return result;
	
	}
	
	/**
	 * runs a SQL query to determine which team has scored the least penalties
	 * 
	 * @throws SQLException
	 */
	public static String leastPenalties() throws SQLException {
		String query = "SELECT team_id, SUM(penalties) from sixnations.scores GROUP BY (team_id) ORDER BY (SUM(tries)) ASC LIMIT 1";
	
		String result;
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
	
		rs.next();
		int teamID = rs.getInt(1);
		int minPenalties = rs.getInt(2);
	
		if (minPenalties < 0) {
	
			result = "No penalties have been scored yet";
	
		} else {
			result = (getTeamName(teamID) + " has the least penalties with: " + minPenalties);
		}
	
		return result;
	}
	
	/**
	 * runs a SQL query to determine which team has the least points
	 * 
	 * @throws SQLException
	 */
	public static String leastPoints() throws SQLException {
		String query = "SELECT team_id, MAX(points_scored) from sixnations.league_table GROUP BY(team_id) ORDER BY(MAX(points_scored)) ASC LIMIT 1 ";
	
		String result;
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
	
		rs.next();
		int teamID = rs.getInt(1);
		int minPoints = rs.getInt(2);
	
		if (minPoints < 0) {
	
			result = "No points have been scored yet";
		} else {
			result = (getTeamName(teamID) + " has the least points scored with: " + minPoints);
		}
	
		return result;
	}

	
	public static String getTeamName(int teamID) {

		String teamName;

		switch (teamID) {

		case 0:
			teamName = "England";
			break;
		case 1:
			teamName = "France";
			break;
		case 2:
			teamName = "Ireland";
			break;
		case 3:
			teamName = "Italy";
			break;
		case 4:
			teamName = "Scotland";
			break;
		case 5:
			teamName = "Wales";
			break;
		default:
			teamName = "Error";

		}

		return teamName;
	}

} // End of DBHelper class.
