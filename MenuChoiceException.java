package master;

/**
 * The MenuChoiceException class handles invalid selections on the Menu screen.
 * It contains a serial version ID, and a method for getting the invalid value.
 * @author <i>Alistair Spence
 * @author Caolan Daly
 * @author Cathal McNulty
 * @author John Corry
 * @author Niall Logue</i>
 * @see Menu.java
 *
 */
public class MenuChoiceException extends Exception
{

	/**
	 * This is the default serial version ID for the exception.
	 */
	private static final long serialVersionUID = 1L;
	
	// Contains the value causing the exception.
	private int menuChoice;
	
	/**
	 * This method can be used to return the invalid value causing the exception. 
	 * This allows for better exception handling in the future.
	 * @return menuChoice: the value which caused the exception.
	 */
	public int getMenuChoice()
	{
		
		return menuChoice;
		
	} // End of getMenuChoice() method.

} // End of MenuChoiceException class.
