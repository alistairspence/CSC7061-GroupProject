package master;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * The Teams class contains a method for creating a HashMap with the values
 * of each team, which can then be used to write to the database.
 * @author <i>Alistair Spence
 * @author Caolan Daly
 * @author Cathal McNulty
 * @author John Corry
 * @author Niall Logue</i>
 *
 */
public class Teams 
{
	
	// players: holds the 15 string values representing players for a given team.
	// The ArrayList is only filled when the fillPlayers() method is called, and cleared 
	// at the beginning of the method.
	protected static ArrayList<String> players = new ArrayList<String>(15);
	
	/**
	 * fillTeams(): creates the HashMap containing the team details, used for
	 * writing the teams to the database.
	 * @return a HashMap containing the team id, and team name
	 */
	public static HashMap<Integer, String> fillTeams()
	{
		
		/*
		int england = 0;
		int france = 1;
		int ireland = 2;
		int italy = 3;
		int scotland = 4;
		int wales = 5;
		*/
		
		// Create the HashMap.
		HashMap<Integer, String> teams = new HashMap<Integer, String>();
		
		// Put the values in the HashMap and return it.
		teams.put(0, "England");
		teams.put(1, "France");
		teams.put(2, "Ireland");
		teams.put(3, "Italy");
		teams.put(4, "Scotland");
		teams.put(5, "Wales");
		
		return teams;
		
	} // End of fillTeams() method.
	
	/**
	 * fillPlayers(): fills the ArrayList with the 15 players for the team
	 * selected by the user. 
	 * @param team: id of the team to use when filling players
	 */
	protected static void fillPlayers(int team)
	{
		
		// Clear any previous data in the ArrayList.
		players.clear();
		
		// Fill the ArrayList with the appropriate players based on the team parameter.
		switch (team)
		{
		
		// England
		case 0:
			players.add("Mike Brown");
			players.add("Jonny May");
			players.add("Johnathan Joseph");
			players.add("Owen Farrell");
			players.add("Elliot Daly");
			players.add("George Ford");
			players.add("Ben Youngs");
			players.add("Joe Marler");
			players.add("Dylan Hartley");
			players.add("Dan Cole");
			players.add("Joe Launchbury");
			players.add("Courtney Lawes");
			players.add("Maro Itoje");
			players.add("Tom Wood");
			players.add("Nathan Hughes");
			break;
			
		// France
		case 1:
			players.add("Scott Spedding");
			players.add("Noa Nakaitaci");
			players.add("Remi Lamerat");
			players.add("Gael Fickou");
			players.add("Virimi Vakatawa");
			players.add("Camille Lopez");
			players.add("Baptiste Serin");
			players.add("Cyril Baille");
			players.add("Guilhem Guirado");
			players.add("Uini Atonio");
			players.add("Sebastien Vahaamahina");
			players.add("Yoann Maestri");
			players.add("Damien Chouly");
			players.add("Kevin Gourdon");
			players.add("Louis Picamoles");
			break;
			
		// Ireland
		case 2:
			players.add("Rob Kearney");
			players.add("Keith Earls");
			players.add("Garry Ringrose");
			players.add("Robbie Henshaw");
			players.add("Simon Zebo");
			players.add("Paddy Jackson");
			players.add("Conor Murray");
			players.add("Jack McGrath");
			players.add("Rory Best");
			players.add("Tadhg Furlong");
			players.add("Iain Henderson");
			players.add("Devin Toner");
			players.add("CJ Stander");
			players.add("Sean O'Brien");
			players.add("Jamie Heaslip");
			break;
			
		// Italy
		case 3:
			players.add("Edoardo Padovani");
			players.add("Giulio Bisegni");
			players.add("Tommaso Benvenuti");
			players.add("Luke McLean");
			players.add("Giovanbattista Venditti");
			players.add("Carlo Canna");
			players.add("Edoardo Gori");
			players.add("Andrea Lovotti");
			players.add("Ornel Gega");
			players.add("Lorenzo Cittadini");
			players.add("Marco Fuser");
			players.add("George Biagi");
			players.add("Braam Steyn");
			players.add("Maxime Mbanda");
			players.add("Sergio Parisse");
			break;
		
		// Scotland
		case 4:
			players.add("Stuart Hogg");
			players.add("Sean Maitland");
			players.add("Huw Jones");
			players.add("Alex Dunbar");
			players.add("Tommy Seymour");
			players.add("Finn Russell");
			players.add("Greig Laidlaw");
			players.add("Allan Dell");
			players.add("Fraser Brown");
			players.add("Zander Fagerson");
			players.add("Richie Gray");
			players.add("Jonny Gray");
			players.add("Ryan Wilson");
			players.add("Hamish Watson");
			players.add("Josh Strauss");
			break;
			
		// Wales
		case 5:
			players.add("Leigh Halfpenny");
			players.add("George North");
			players.add("Jonathan Davies");
			players.add("Scott Williams");
			players.add("Liam Williams");
			players.add("Dan Biggar");
			players.add("Rhys Webb");
			players.add("Nicky Smith");
			players.add("Ken Owens");
			players.add("Samson Lee");
			players.add("Jake Ball");
			players.add("Alun Wyn Jones");
			players.add("Sam Warburton");
			players.add("Justin Tipuric");
			players.add("Ross Moriarty");
			break;
			
		// Should never happen!
		default:
			System.out.println("Error viewing players. Please try again.");
			break;
			
		} // End of switch statement.
		
	} // End of fillPlayers() method.
	
	/**
	 * printPlayers(): this method calls the fillPlayers() method to fill the
	 * players ArrayList, then prints the individual players to console.
	 * @param team: this int is passed to the fillPlayers() method to fill the ArrayList
	 * 				with the correct players.
	 */
	protected static void printPlayers(int team)
	{	
		
		// Fill the ArrayList.
		fillPlayers(team);
		
		// Formatting the header.
		System.out.println();
		System.out.println("   " + DBHelper.getTeamName(team));
		System.out.println();
		
		// For each player in the ArrayList, print the string.
		for (String player : players)
		{
			
			System.out.println(player);
			
		} // End for loop.
		
		System.out.println();
		
	} // End of printPlayers() method.

} // End of Teams class.
