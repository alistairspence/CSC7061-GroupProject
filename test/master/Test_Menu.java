package master;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Test_Menu {

	// Test data
	private static ByteArrayInputStream testInput;
	private static String expectedOutput; 
	
	// TODO(alistair): for validating ranges, use values either side of the boundary (0, 1 + 5, 6)
	// instead of 0, 3, 6
	// TODO(alistair): InputMismatchEXCEPTION
	// TODO(alistair): set DEBUG_STRING to empty in the setUpBeforeClass?
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
		
		Menu.DEBUG_MODE = true;
		
	}

	@Before
	public void setUp() throws Exception 
	{
		
		System.setIn(System.in);
		
	}

	@After
	public void tearDown() throws Exception 
	{	
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
		
		Menu.DEBUG_MODE = false;
		
	}

	@Test
	public void testDisplayMenu() throws IOException
	{
				
		// This creates a stream to simulate the user selecting the quit option.
		// setIn() then sets the input stream to the newly created stream.
		testInput = new ByteArrayInputStream("8".getBytes());
		System.setIn(testInput);
	
		// Set the TEST_ID.
		Menu.TEST_ID = 1;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code 
		// performs as intended.
		expectedOutput = "Menu displayed!";
				
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}
	
	@Test
	public void testDisplayMenuInvalidChoiceLow() 
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 2;

		// We will expect this to be the value of Menu.DEBUG_STRING if the code 
		// performs as intended.
		expectedOutput = "Invalid menu choice - too low!";
		
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);

	}
	
	@Test
	public void testDisplayMenuInvalidChoiceHigh() 
	{
				
		// Set the TEST_ID.
		Menu.TEST_ID = 3;
			
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Invalid menu choice - too high!";
		
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
	
	}
	
	@Test (expected = NumberFormatException.class)
	public void testDisplayMenuInputMismatch()
	{

		// Set the TEST_ID.
		Menu.TEST_ID = 4;
		
		// Display the menu.
		Menu.displayMenu();
		
	}
	
	@Test
	public void testViewFixtures()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 5;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Viewing fixtures!";
		
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}

	@Test
	public void testEnterMatchScores()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 6;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Entering scores!";
		
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}
	
	@Test
	public void testViewRoundScores()
	{
	
		// Set the TEST_ID.
		Menu.TEST_ID = 7;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Viewing round!";
		
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}
	
	@Test
	public void testViewRoundScoresInvalidChoiceLow()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 8;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Invalid round choice - too low!";
		
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}
	
	@Test
	public void testViewRoundScoresInvalidChoiceHigh()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 9;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Invalid round choice - too high!";
		
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}
	
	@Test (expected = NumberFormatException.class)
	public void testViewRoundScoresInputMismatchException()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 10;
		
		// Display the menu.
		Menu.displayMenu();

	}
	
	@Test
	public void testUploadRoundScores()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 11;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Round scores uploaded!";
		
		// Display the menu.
		Menu.displayMenu();

		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}
	
	@Test
	public void testUploadRoundScoresInvalidChoiceLow()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 12;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Invalid round choice - too low!";
		
		// Display the menu.
		Menu.displayMenu();

		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}
	
	@Test
	public void testUploadRoundScoresInvalidChoiceHigh()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 13;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Invalid round choice - too high!";
		
		// Display the menu.
		Menu.displayMenu();

		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}
	
	@Test (expected = NumberFormatException.class)
	public void testUploadRoundScoresInputMismatchException()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 14;
		
		// Display the menu.
		Menu.displayMenu();
		
	}
	
	@Test
	public void testViewLeagueTable()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 15;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Viewing table!";
		
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}
	
	@Test
	public void testSearchDB()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 16;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Searching!";
		
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}

	@Test
	public void testViewPlayers()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 17;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Viewing players!";
		
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}
	
	@Test
	public void testViewPlayersInvalidChoiceLow()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 18;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Invalid team choice - too low!";
		
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}
	
	@Test
	public void testViewPlayersInvalidChoiceHigh()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 19;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Invalid team choice - too high!";
		
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}
	
	@Test (expected = NumberFormatException.class)
	public void testViewPlayersInputMismatchException()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 20;
		
		// Display the menu.
		Menu.displayMenu();

	}
	
	@Test
	public void testQuit()
	{
		
		// Set the TEST_ID.
		Menu.TEST_ID = 21;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Quitting!";
		
		// Display the menu.
		Menu.displayMenu();
		
		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Menu.DEBUG_STRING);
		
	}
	
}
