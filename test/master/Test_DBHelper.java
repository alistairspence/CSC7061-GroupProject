package master;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Scanner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Test_DBHelper 
{

	// test data
	private static Connection con;

	private static final int NUMBER_OF_TEAMS = 6;

	protected static int currentMatchID, currentTeamID;
	// public static int calculateBonusPoints, totalTries, totalPointsScored,
	// matchPoints, pointsDifference, pointsConceded, gamesPlayed;

	String password, username, url, allLeagueTable, createLeagueTable, SQL, deleteTeams, deleteFixtures,
			// previousRoundMatches, fixturesTable, deleteDuplicates,
			// homeTeamScores, awayTeamScores, gamesPlayed, concededPoints,
			// pointsDifference,
			// totalPoints, leaguePoints, setWin, setDraw, totalTries,
			// bonusPoints, SQL, sequel, leaguePoints, leagueWithBonus,
			// homeTeamScores,
			// opponentPoints, leagueTableBeforeDelete, updateLeagueTable,
			// isMatchComplete,getLeagueTable, grandSlam, updateSlamTotal,
			// updateSlamBonus,
			teamName, team1, team2, userFilepath, roundString, roundQuery, query, games; // teamName;

	int match, teamIndex, result, round, currentRound, scoresEntered, matchID, teamID1, teamID2, tries1, tries2,
			conversions1, conversions2, penalties1, penalties2, points1, points2, awayTeamID, homeTeamID, pointsFor,
			pointsAgaints, newGamesPlayed, newConcededPoints, newTotalPointsScored,
			// newPointsDifference, , newTotalPoints,
			// newTotalTries, pointsDiff, bonusPointsGained,
			// totalBonusPoints, newTotalPoints, matchTries, matchBonusPoints,
			// matchPointsScored, matchLeaguePoints, opponentMatchPoints,
			// newGamesPlayed, newPointsScored, newPointsConceded,
			// newPointsDifference, newTriesScored,newBonusPoints,
			// newTotalPoints, decision,
			totalWithBonus, totalBonusPoints, lowMatch, highMatch, searchChoice, choice, choice2, mostTries,
			mostConversions, mostPenalties, mostPoints, minTries, minConversions, minPenalties, minPoints, pointsScored;

	Statement getLeague, updateLeague, st, deleteStatement, writeFixtures, roundScoresFinished, getFix, homeID, awayID,
			clearDuplicates, getGames, getPoints, getPointsDifference, getTotalPointsScored, getLeaguePoints, win;
	// draw, getTotalTries, getBonusPoints, getLeaguePoints, leagueAndBonus,
	// leagueTable, scoresTable, opponentPoint, update, completeMatch,
	// showLeagueTable, grande, updateTotal, roundStatement, st, sta;

	ResultSet emptyLeague, results, roundComplete, fixes, homeTeam, awayTeam, played, points, pointsDiff,
			// totPointsScored, totalPoints, totTriesScored, totBonusPoints,
			// totalPoints, currentLeagueTable, scoresUndo, opponentScored,
			// matchEntered, rsLeague, winner,
			rs, rsGames;

	ResultSetMetaData rsmd;

	boolean resultBool, searchMenuInUse;

	double gamesWon, gamesPlayed, winPercentage, gamesDrawn, gamesLost, lossPercentage;

	HashMap<Integer, String> teams = Teams.fillTeams();
	Integer teamID;

	DBHelper DBH;

	@Before
	public void setUp() throws Exception {
		DBH = new DBHelper();
		url = "jdbc:mysql://rds-mysql-sixnations.c4jxximab4x4.eu-west-1.rds.amazonaws.com:3306";
		username = "sixnations";
		password = "password";
		con = null;
		pointsScored = 10;
		con = DriverManager.getConnection(url, username, password);
		DBHelper.getRemoteConnection();
		Class.forName("com.mysql.jdbc.Driver");

	}

	@Test
	public void testGetRemoteConnection() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url, username, password);

		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection expected = con;

		assertNotEquals(expected, DBHelper.getRemoteConnection());

	}

	@Test
	public void testRemoteConnectionNotNull() {

		try {
			con = DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertNotNull(DBHelper.getRemoteConnection());
		assertNotNull(con);

	}

	@Test(expected = SQLException.class)
	public void testSQLException() throws SQLException {
		con = DriverManager.getConnection("", username, password);
	}

	@Test(expected = ClassNotFoundException.class)
	public void testClassNotFoundException() throws SQLException, ClassNotFoundException {

		con = DriverManager.getConnection(url, username, password);
		Class.forName("");

	}

	@Test
	public void testReadFromFixturesHome() {
		int roundChoice = 1;
		match = 1;
		String expected = "5";
		String actual = "";
		DBHelper.readFromFixtures(roundChoice);
		SQL = "SELECT team1_id FROM sixnations.fixtures WHERE match_id = " + (match + 1);

		try {
			Statement st = con.createStatement();
			ResultSet results = st.executeQuery(SQL);
			while (results.next()) {
				actual = results.getString("team1_id");
				assertEquals(expected, actual);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	@Test
	public void testReadFromFixturesAway() {
		int roundChoice = 1;
		match = 1;
		String expected = "2";
		String actual = "";
		DBHelper.readFromFixtures(roundChoice);
		SQL = "SELECT team2_id FROM sixnations.fixtures WHERE match_id = " + (match + 1);

		try {
			Statement st = con.createStatement();
			ResultSet results = st.executeQuery(SQL);
			while (results.next()) {
				actual = results.getString("team2_id");
				assertEquals(expected, actual);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		System.out.println(expected);
		System.out.println(actual);

	}

	@Test
	public void testClearDB() {
		String expected = null;
		String actual;
		DBHelper.clearDB();

		try {
			String checkFixturesAreDeleted = "SELECT * FROM sixnations.fixtures";
			String checkTeamsAreDeleted = "SELECT * FROM sixnations.teams";
			Statement checkFixtures = con.createStatement();
			Statement checkTeams = con.createStatement();
			ResultSet rsFixtures = checkFixtures.executeQuery(checkFixturesAreDeleted);
			ResultSet rsTeams = checkTeams.executeQuery(checkTeamsAreDeleted);

			while (rsFixtures.next()) {
				actual = rsFixtures.getString("0");
				assertEquals(expected, actual);
			}
			while (rsTeams.next()) {
				actual = rsTeams.getString("0");
				assertEquals(expected, actual);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// not working yet
	@Test
	public void testDeleteTeams() {

		Object expected = null;
		Object actual;

		try {
			deleteTeams = "DELETE FROM sixnations.teams";
			Statement deleteStatement = con.createStatement();
			deleteStatement.executeUpdate(deleteTeams);

			String checkTeamsAreDeleted = "SELECT * FROM sixnations.teams";
			Statement check = con.createStatement();
			ResultSet rs = check.executeQuery(checkTeamsAreDeleted);
			for (rs.next();;) {
				actual = (Integer) rs.getObject(1);
				assertEquals(expected, actual);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testDeleteFixtures() {
		Object expected = null;
		Object actual;

		try {
			deleteFixtures = "DELETE FROM sixnations.fixtures";
			Statement deleteStatement = con.createStatement();
			deleteStatement.executeUpdate(deleteFixtures);

			String checkFixturesAreDeleted = "SELECT * FROM sixnations.fixtures";
			Statement check = con.createStatement();
			ResultSet rs = check.executeQuery(checkFixturesAreDeleted);
			for (rs.next();;) {
				actual = (Integer) rs.getObject(1);
				assertEquals(expected, actual);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// TODO - come back to as too complicated

	@Test
	public void testGamesPlayed() {
		teamID = 1;
		String gamesPlayed = "SELECT games_played from sixnations.league_table WHERE team_id = " + teamID;
		int actual = 0;
		int expectedGamesPlayed = 5;
		int expected = (expectedGamesPlayed + 1);
		DBHelper.gamesPlayed(teamID);

		try {
			Statement getGames = con.createStatement();
			ResultSet played = getGames.executeQuery(gamesPlayed);
			while (played.next()) {
				actual = played.getInt("games_played") + 1;
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		assertEquals(expected, actual);
		System.out.println(expected);
		System.out.println(actual);
	}

	@Test
	public void testPointsConceded() {
		teamID = 1;
		String concededPoints = "SELECT points_conceded from sixnations.league_table WHERE team_id = " + teamID;
		int actual = 0;
		int expectedPointsConceded = 163;
		int opponentPoints = 37;
		int expected = (expectedPointsConceded + opponentPoints);
		DBHelper.pointsConceded(teamID, opponentPoints);
		try {
			Statement getPoints = con.createStatement();
			ResultSet points = getPoints.executeQuery(concededPoints);
			while (points.next()) {
				actual = points.getInt("points_conceded") + opponentPoints;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

		assertEquals(expected, actual);
		System.out.println(expected);
		System.out.println(actual);

	}

	@Test
	public void testPointsDifference() {
		teamID = 1;
		String pointsDifference = "SELECT points_difference from sixnations.league_table WHERE team_id = " + teamID;
		int actual = 0;
		int expectedPointsDiff = -15;
		int pointsAgainst = 15;
		int pointsFor = 20;
		int expected = (expectedPointsDiff + (pointsFor - pointsAgainst));
		DBHelper.pointsDifference(teamID, pointsFor, pointsAgainst);

		try {
			Statement getPointsDifference = con.createStatement();
			ResultSet pointsDiff = getPointsDifference.executeQuery(pointsDifference);
			while (pointsDiff.next()) {

				actual = pointsDiff.getInt("points_difference") + (pointsFor - pointsAgainst);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		assertEquals(expected, actual);
	}

	@Test
	public void testTotalPointsScored() {
		teamID = 1;
		int actual = 0;
		int expectedPointsScored = 50;
		int pointsScored = 50;
		int expected = (expectedPointsScored + pointsScored);
		DBHelper.totalPointsScored(teamID, pointsScored);
		String totalPoints = "SELECT points_scored from sixnations.league_table WHERE team_id = " + teamID;
		try {
			Statement getTotalPointsScored = con.createStatement();
			ResultSet totPointsScored = getTotalPointsScored.executeQuery(totalPoints);
			while (totPointsScored.next()) {
				actual = (totPointsScored.getInt("points_scored") + pointsScored);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		assertEquals(expected, actual);
	}

	@Test
	public void testSetWin() {
		teamID = 1;
		currentMatchID = 1;
		String expected = "true";
		String actual = "";
		newTotalPointsScored = 50;
		int pointsConceded = 20;
		DBHelper.setWin(1, newTotalPointsScored, pointsConceded);
		String checkWin = "SELECT (win) FROM sixnations.scores  WHERE team_id = " + teamID + " AND match_id = "
				+ currentMatchID;
		try {
			Statement checkWinner = con.createStatement();
			ResultSet check = checkWinner.executeQuery(checkWin);
			while (check.next()) {
				actual = check.getString("win");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testSetDraw() {
		teamID = 1;
		currentMatchID = 1;
		String expected = "true";
		String actual = "";
		newTotalPointsScored = 50;
		int pointsConceded = 50;
		DBHelper.getRemoteConnection();
		DBHelper.setDraw(1, newTotalPointsScored, pointsConceded);
		String checkDraw = "SELECT (draw) FROM sixnations.scores  WHERE team_id = " + teamID + " AND match_id = "
				+ currentMatchID;
		Statement Draw;
		try {
			Draw = con.createStatement();
			ResultSet check = Draw.executeQuery(checkDraw);
			while (check.next()) {
				actual = check.getString("win");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testTotalTries() {
		teamID = 1;
		String current = "16";
		int triesScored = 5;
		String expected = (current + triesScored);
		String actual = "";
		DBHelper.totalTries(teamID, triesScored);
		String checkTries = "SELECT (tries_scored) FROM sixnations.league_table  WHERE team_id = " + teamID;
		try {
			Statement tries = con.createStatement();
			ResultSet check = tries.executeQuery(checkTries);
			while (check.next()) {
				actual = check.getString("tries_scored") + triesScored;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(expected, actual);
	}

	@Test
	public void testAddBonusPointsToLeague() {
		// current changes every time this runs, so +3 everytime

		teamID = 1;
		currentMatchID = 1;
		String current = "24";
		int bonusPoints = 3;
		String actual = "";
		String expected = (current + bonusPoints);

		DBHelper.addBonusPointsToLeague(teamID, bonusPoints);
		String checkPoints = "SELECT (total_points) FROM sixnations.league_table  WHERE team_id = " + teamID;
		try {
			Statement bonus = con.createStatement();
			ResultSet check = bonus.executeQuery(checkPoints);
			while (check.next()) {
				actual = check.getString("total_points") + bonusPoints;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(expected, actual);
	}

	@Test
	public void testIsMatchEnteredNo() {
		currentMatchID = 1;
		String expected = "0";
		String actual = "";
		DBHelper.isMatchEntered(currentMatchID);
		String isMatchComplete = "SELECT COUNT(*) FROM sixnations.scores WHERE match_id = " + currentMatchID;
		try {
			Statement isMatch = con.createStatement();
			ResultSet check = isMatch.executeQuery(isMatchComplete);
			while (check.next()) {
				actual = check.getString(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(expected, actual);

	}

	// TODO not sure how to test this
	@Test
	public void testIsMatchEnteredYes() {
		currentMatchID = 2;
		String expected = "1";
		String actual = "";
		DBHelper.isMatchEntered(currentMatchID);
		String isMatchComplete = "SELECT COUNT(*) FROM sixnations.scores WHERE match_id = " + currentMatchID;
		try {
			Statement isMatch = con.createStatement();
			ResultSet check = isMatch.executeQuery(isMatchComplete);
			while (check.next()) {
				actual = check.getString(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(expected, actual);

	}

	@Test
	public void testGenerateTableColumnNames() {
		String expected = "";
		String actual = "";
		DBHelper.generateTable();
		String getLeagueTable = "SELECT * FROM sixnations.league_table ORDER BY (total_points) DESC";
		Statement showLeagueTable;
		try {
			showLeagueTable = con.createStatement();
			showLeagueTable.executeQuery(getLeagueTable);
			ResultSet rsLeague = showLeagueTable.getResultSet();
			ResultSetMetaData rsmd = rsLeague.getMetaData();
			String[] array = { "team_id", "games_played", "tries_scored", "points_scored", "points_conceded",
					"points_difference", "tries_scored", "bonus_points", "total_points" };
			System.out.println("expected");

			for (int loop = 0; loop < array.length; loop++) {
				expected = array[loop];
				System.out.println(expected);

			}
			System.out.println("");
			System.out.println("actual");

			for (int i = 1; i <= rsmd.getColumnCount(); i++) {
				actual = rsmd.getColumnName(i);
				System.out.println(actual);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(expected, actual);
	}

	@Test
	public void testWriteToLeagueTable() {
		int team_id = 2;
		int games_played = 2;
		int points_scored = 60;
		int points_conceded = 30;
		int points_difference = (points_scored - points_conceded);
		int tries_scored = 5;
		int bonus_points = 1;
		int total_points = 4;
		String actual = "";
		String expected = "";

		DBHelper.writeToLeagueTable(team_id, games_played, points_scored, points_conceded, points_difference,
				tries_scored, bonus_points, total_points);
		String checkLeague = "SELECT * FROM sixnations.league_table WHERE team_id = " + team_id;
		try {
			Statement league = con.createStatement();
			ResultSet check = league.executeQuery(checkLeague);
			while (check.next()) {
				actual = check.getString(1);
				expected = "2";
				assertEquals(expected, actual);

				actual = check.getString(2);
				expected = "2";
				assertEquals(expected, actual);

				actual = check.getString(3);
				expected = "60";
				assertEquals(expected, actual);

				actual = check.getString(4);
				expected = "30";
				assertEquals(expected, actual);

				actual = check.getString(5);
				expected = "30";
				assertEquals(expected, actual);

				actual = check.getString(6);
				expected = "5";
				assertEquals(expected, actual);

				actual = check.getString(7);
				expected = "1";
				assertEquals(expected, actual);

				actual = check.getString(8);
				expected = "4";
				assertEquals(expected, actual);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testScanner() {
		int expected = 5;
		int actual;
		Scanner s = new Scanner(System.in);
		actual = s.nextInt();
		assertEquals(actual, expected);
		s.close();
	}

	@Test
	public void testInvalidThenValidRound() {
		int round = 0;
		Scanner s = new Scanner(System.in);
		System.out.println("Which Round would you like to enter scores for: ");
		round = 6;
		int expected = 6;
		while (round < 1 || round > 5) {

			System.out.println("Not a Valid Round - Select 1-5");
			round = 5;
		}
		expected = 5;
		assertEquals(round, expected);

	}

	@Test
	public void previousRoundMatches() {
		int round = 1;
		// expected = 3 as the String previousRoundMatches returns the previous
		// 3 rounds
		int expected = 3;
		int actual;
		try {
			String previousRoundMatches = "SELECT COUNT(*) FROM sixnations.scores WHERE match_id <=  " + (round * 3 - 3)
					+ " AND match_id >=  " + (round * 3 - 5);
			Statement roundScoresFinished = con.createStatement();
			ResultSet roundComplete = roundScoresFinished.executeQuery(previousRoundMatches);

			for (roundComplete.next();;) {
				actual = (Integer) roundComplete.getObject(1);
				assertEquals(expected, actual);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testWriteToScores() {
		int round = 1;
		int matchID = 1;
		int tries1 = 2;
		int tries2 = 4;
		int conversions1 = 2;
		int conversions2 = 1;
		int penalties1 = 1;
		int penalties2 = 3;
		int points1 = 17;
		int points2 = 31;
		int teamID1 = 2;
		int teamID2 = 5;
		String actual1 = "";
		String expected1 = "";
		String actual2 = "";
		String expected2 = "";
		Boolean win1 = false;
		Boolean win2 = true;
		Boolean draw1 = false;
		Boolean draw2 = false;
		DBHelper.writeToScores();

		String SQL = "INSERT INTO sixnations.scores VALUES(" + matchID + ", " + teamID1 + ", " + tries1 + ", "
				+ conversions1 + ", " + penalties1 + ", " + points1 + ", " + win1 + "," + draw1 + ")";
		try {
			Statement enterScores1 = con.createStatement();
			enterScores1.executeUpdate(SQL);

			String viewScoresTable1 = "Select * sixnations.scores WHERE match_id = " + matchID + " AND team_id = "
					+ teamID1 + ")";
			Statement viewScores1 = con.createStatement();
			ResultSet view1 = viewScores1.executeQuery(viewScoresTable1);
			while (view1.next()) {
				actual1 = view1.getString(1);
				expected1 = "1";
				assertEquals(expected1, actual1);
				System.out.println(expected1);
				System.out.println(actual1);

				actual1 = view1.getString(2);
				expected1 = "2";
				assertEquals(expected1, actual1);
				System.out.println(expected1);
				System.out.println(actual1);

				actual1 = view1.getString(3);
				expected1 = "2";
				assertEquals(expected1, actual1);
				System.out.println(expected1);
				System.out.println(actual1);

				actual1 = view1.getString(4);
				expected1 = "2";
				assertEquals(expected1, actual1);
				System.out.println(expected1);
				System.out.println(actual1);

				actual1 = view1.getString(5);
				expected1 = "1";
				assertEquals(expected1, actual1);
				System.out.println(expected1);
				System.out.println(actual1);

				actual1 = view1.getString(6);
				expected1 = "17";
				assertEquals(expected1, actual1);
				System.out.println(expected1);
				System.out.println(actual1);

				actual1 = view1.getString(7);
				expected1 = "false";
				assertEquals(expected1, actual1);
				System.out.println(expected1);
				System.out.println(actual1);

				actual1 = view1.getString(8);
				expected1 = "false";
				assertEquals(expected1, actual1);
				System.out.println(expected1);
				System.out.println(actual1);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String squeuel = "INSERT INTO sixnations.scores VALUES(" + matchID + ", " + teamID2 + ", " + tries2 + ", "
				+ conversions2 + ", " + penalties2 + ", " + points2 + ", " + win2 + "," + draw2 + ";";

		try {
			Statement enterScores2 = con.createStatement();
			ResultSet check = enterScores2.executeQuery(squeuel);

			String viewScoresTable2 = "Select * sixnations.scores WHERE match_id = " + matchID + " AND team_id = "
					+ teamID2 + ")";
			Statement viewScores2 = con.createStatement();
			ResultSet view2 = viewScores2.executeQuery(viewScoresTable2);

			while (view2.next()) {
				actual2 = check.getString(1);
				expected2 = "1";
				assertEquals(expected2, actual2);

				actual2 = view2.getString(2);
				expected2 = "5";
				assertEquals(expected2, actual2);

				actual2 = view2.getString(3);
				expected2 = "4";
				assertEquals(expected2, actual2);

				actual2 = view2.getString(4);
				expected2 = "1";
				assertEquals(expected2, actual2);

				actual2 = view2.getString(5);
				expected2 = "3";
				assertEquals(expected2, actual2);

				actual2 = view2.getString(6);
				expected2 = "31";
				assertEquals(expected2, actual2);

				actual2 = view2.getString(7);
				expected2 = "true";
				assertEquals(expected2, actual2);

				actual2 = view2.getString(8);
				expected2 = "false";
				assertEquals(expected2, actual2);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void removeDuplicateScores() {
		currentMatchID = 1;
		currentTeamID = 1;

		try {

			String deleteDuplicates = "DELETE FROM sixnations.scores WHERE match_id = " + currentMatchID;

			String homeTeamScores = "SELECT * FROM sixnations.scores WHERE match_id = " + currentMatchID
					+ " AND team_id = " + currentTeamID;

			Statement homeID = con.createStatement();
			int homeTeamID = -1;
			ResultSet homeTeam = homeID.executeQuery(homeTeamScores);
			while (homeTeam.next()) {
				homeTeamID = homeTeam.getInt("team_id");
			}

			DBHelper.undoLeagueTableAfterDelete(currentMatchID, homeTeamID);

			Statement clearDuplicates = con.createStatement();
			clearDuplicates.executeUpdate(deleteDuplicates);
			Menu.displayMenu();
		} catch (SQLException s) {

			s.printStackTrace();
		}
	}

	@SuppressWarnings("static-access")
	@Test
	public void testGetTeamNameOne() {
		int teamID = 0;
		String expected = "England";
		assertEquals(expected, DBH.getTeamName(teamID));
	}

	@SuppressWarnings("static-access")
	@Test
	public void testGetTeamNameTwo() {
		int teamID = 1;
		String expected = "France";
		assertEquals(expected, DBH.getTeamName(teamID));
	}

	@SuppressWarnings("static-access")
	@Test
	public void testGetTeamNameThree() {
		int teamID = 2;
		String expected = "Ireland";
		assertEquals(expected, DBH.getTeamName(teamID));
	}

	@SuppressWarnings("static-access")
	@Test
	public void testGetTeamNameFour() {
		int teamID = 3;
		String expected = "Italy";
		assertEquals(expected, DBH.getTeamName(teamID));
	}

	@SuppressWarnings("static-access")
	@Test
	public void testGetTeamNameFive() {
		int teamID = 4;
		String expected = "Scotland";
		assertEquals(expected, DBH.getTeamName(teamID));
	}

	@SuppressWarnings("static-access")
	@Test
	public void testGetTeamNameSix() {
		int teamID = 5;
		String expected = "Wales";
		assertEquals(expected, DBH.getTeamName(teamID));
	}

	@SuppressWarnings("static-access")
	@Test
	public void testGetTeamNameDefaultLower() {
		int teamID = -1;
		String expected = "Error";
		assertEquals(expected, DBH.getTeamName(teamID));
	}

	@SuppressWarnings("static-access")
	@Test
	public void testGetTeamNameDefaultUpper() {
		int teamID = 6;
		String expected = "Error";
		assertEquals(expected, DBH.getTeamName(teamID));
	}

	@After
	public void tearDown() {
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	

	@Test
	public void testWinPercentage() {
		// update code to put test data into database.
		String update = "UPDATE sixnations.scores SET win  = true  WHERE team_id = 1";
		String update2 = "UPDATE sixnations.league_table SET games_played  = 5  WHERE team_id = 1";
		try {

			// executing update for test data
			Statement st = con.createStatement();
			st.execute(update);
			Statement st2 = con.createStatement();
			st2.execute(update2);
			// teamID 1 = France
			// expected number of tries
			double winpercentage = 100.00;

			// the comparison of the results
			String expected = "France has won " + winpercentage + "% of 5 game(s)";
			String actual = DBHelper.winPercentage(1);
			assertEquals(expected, actual);

			// reverts the scores back to zero (removes the test data from the
			// database)
			String update3 = "UPDATE sixnations.scores SET win  = false  WHERE team_id = 1";
			String update4 = "UPDATE sixnations.league_table SET games_played  = 0  WHERE team_id = 1";
			Statement st3 = con.createStatement();
			st3.execute(update3);
			Statement st4 = con.createStatement();
			st4.execute(update4);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	

	@Test
	public void testMostTries() {

		// update code to put test data into database.
		String update = "UPDATE sixnations.scores SET tries  = 10 WHERE team_id = 1";
		try {

			// executing update for test data
			Statement st = con.createStatement();
			st.execute(update);
			// teamID 1 = France so looking for france to come back with the
			// most tries
			// expected number of tries
			int triesExpected = 50;

			// the comparison of the results
			String expected = "France has the most tries with: " + triesExpected;
			String actual = DBHelper.mostTries();
			assertEquals(expected, actual);

			// reverts the scores back to zero (removes the test data from the
			// database)
			String update2 = "UPDATE sixnations.scores SET tries  = 0 WHERE team_id = 1";
			Statement st2 = con.createStatement();
			st2.execute(update2);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void testMostConversions() {
		// update code to put test data into database.
		String update = "UPDATE sixnations.scores SET tries = 10, conversions  = 10 WHERE team_id = 1";
		try {

			// executing update for test data
			Statement st = con.createStatement();
			st.execute(update);
			// teamID 1 = France so looking for france to come back with the
			// most conversions
			// expected number of tries
			int conversionsExpected = 50;

			// the comparison of the results
			String expected = "France has the most conversions with: " + conversionsExpected;
			String actual = DBHelper.mostConversions();
			assertEquals(expected, actual);

			System.out.println(actual);
			// reverts the scores back to zero (removes the test data from the
			// database)
			String update2 = "UPDATE sixnations.scores SET conversions  = 0 , tries = 0 WHERE team_id = 1";
			Statement st2 = con.createStatement();
			st2.execute(update2);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void testMostPenalties() {
		// update code to put test data into database.
		String update = "UPDATE sixnations.scores SET penalties  = 10 WHERE team_id = 1";
		try {

			// executing update for test data
			Statement st = con.createStatement();
			st.execute(update);
			// teamID 1 = France so looking for france to come back with the
			// most penalties
			// expected number of penalties
			int penaltiesExpected = 50;

			// the comparison of the results
			String expected = "France has the most penalties with: " + penaltiesExpected;
			String actual = DBHelper.mostPenalties();
			assertEquals(expected, actual);

			// reverts the scores back to zero (removes the test data from the
			// database)
			String update2 = "UPDATE sixnations.scores SET penalties  = 0 WHERE team_id = 1";
			Statement st2 = con.createStatement();
			st2.execute(update2);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testMostPoints() {
		// update code to put test data into database.
		String update = "UPDATE sixnations.league_table SET points_scored  = 500 WHERE team_id = 1";
		try {

			// executing update for test data
			Statement st = con.createStatement();
			st.execute(update);
			// teamID 1 = France so looking for france to come back with the
			// most points
			// expected number of points
			int pointsExpected = 500;

			// the comparison of the results
			String expected = "France has the most points scored with: " + pointsExpected;
			String actual = DBHelper.mostPoints();
			assertEquals(expected, actual);

			// reverts the scores back to zero (removes the test data from the
			// database)
			String update2 = "UPDATE sixnations.league_table SET points_scored  = 0 WHERE team_id = 1";
			Statement st2 = con.createStatement();
			st2.execute(update2);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testLeastTries() {
		// update code to put test data into database.
		String update = "UPDATE sixnations.scores SET tries  = -10 WHERE team_id = 1";
		try {

			// executing update for test data
			Statement st = con.createStatement();
			st.execute(update);
			// teamID 1 = France so looking for france to come back with the
			// least tries
			// expected number of tries
			int triesExpected = -50;

			// the comparison of the results
			String expected = "France has the least tries with: " + triesExpected;
			String actual = DBHelper.leastTries();
			assertEquals(expected, actual);

			// reverts the scores back to zero (removes the test data from the
			// database)
			String update2 = "UPDATE sixnations.scores SET tries  = 0 WHERE team_id = 1";
			Statement st2 = con.createStatement();
			st2.execute(update2);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testLeastConversions() {
		// update code to put test data into database.
		String update = "UPDATE sixnations.scores SET conversions  = -10 WHERE team_id = 1";
		try {

			// executing update for test data
			Statement st = con.createStatement();
			st.execute(update);
			// teamID 1 = France so looking for france to come back with the
			// least conversions
			// expected number of conversions
			int conversionsExpected = -50;

			// the comparison of the results
			String expected = "France has the least conversions with: " + conversionsExpected;
			String actual = DBHelper.leastConversions();
			assertEquals(expected, actual);

			// reverts the scores back to zero (removes the test data from the
			// database)
			String update2 = "UPDATE sixnations.scores SET conversions  = 0 WHERE team_id = 1";
			Statement st2 = con.createStatement();
			st2.execute(update2);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void testLeastPenalties() {
		// update code to put test data into database.
		String update = "UPDATE sixnations.scores SET penalties  = -10 WHERE team_id = 1";
		try {

			// executing update for test data
			Statement st = con.createStatement();
			st.execute(update);
			// teamID 1 = France so looking for france to come back with the
			// least penalties
			// expected number of penalties
			int penaltiesExpected = -50;

			// the comparison of the results
			String expected = "France has the least penalties with: " + penaltiesExpected;
			String actual = DBHelper.leastPenalties();
			assertEquals(expected, actual);

			// reverts the scores back to zero (removes the test data from the
			// database)
			String update2 = "UPDATE sixnations.scores SET penalties  = 0 WHERE team_id = 1";
			Statement st2 = con.createStatement();
			st2.execute(update2);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testLeastPoints() {
		// update code to put test data into database.
		String update = "UPDATE sixnations.league_table SET points_scored  = 0 WHERE team_id = 1";
		try {

			// executing update for test data
			Statement st = con.createStatement();
			st.execute(update);
			// teamID 1 = France so looking for france to come back with the
			// least points
			// expected number of points
			int pointsExpected = 0;

			// the comparison of the results
			String expected = "France has the least points scored with: " + pointsExpected;
			String actual = DBHelper.leastPoints();
			assertEquals(expected, actual);

			// reverts the scores back to zero (removes the test data from the
			// database)
			String update2 = "UPDATE sixnations.league_table SET points_scored  = 0 WHERE team_id = 1";
			Statement st2 = con.createStatement();
			st2.execute(update2);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetTeamName1() {
		String actual = DBHelper.getTeamName(0);
		String expected = "England";
		assertEquals(expected, actual);
	}

	@Test
	public void testGetTeamName2() {
		String actual = DBHelper.getTeamName(1);
		String expected = "France";
		assertEquals(expected, actual);
	}

	@Test
	public void testGetTeamName3() {
		String actual = DBHelper.getTeamName(2);
		String expected = "Ireland";
		assertEquals(expected, actual);
	}

	@Test
	public void testGetTeamName4() {
		String actual = DBHelper.getTeamName(3);
		String expected = "Italy";
		assertEquals(expected, actual);
	}

	@Test
	public void testGetTeamName5() {
		String actual = DBHelper.getTeamName(4);
		String expected = "Scotland";
		assertEquals(expected, actual);
	}

	@Test
	public void testGetTeamName6() {
		String actual = DBHelper.getTeamName(5);
		String expected = "Whales";
		assertEquals(expected, actual);
	}

	@Test
	public void testGetTeamNameError() {
		String actual = DBHelper.getTeamName(6);
		String expected = "Error";
		assertEquals(expected, actual);
	}
	@Test
	public void testWriteToTeams() {
		fail("Not yet implemented");
	}

	@Test
	public void testReadFromFixturesInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testReadFromScores() {
		fail("Not yet implemented");
	}

	@Test
	public void testUploadRoundScoresWindows() {
		fail("Not yet implemented");
	}

	@Test
	public void testUploadRoundScoresMac() {
		fail("Not yet implemented");
	}

	@Test
	public void testSearchDB() {
		fail("Not yet implemented");
	}

	@Test
	public void testPrintTeamDetails() {
	}

	@Test
	public void testIsDBInitialised() {
		fail("Not yet implemented");
	}

	@Test
	public void testGenerateTableData() {
	}

	@Test
	public void testUndoLeagueTableAfterDelete() {
		currentMatchID = 1;
		teamID = 2;
		DBHelper.undoLeagueTableAfterDelete(currentMatchID, teamID);
	}

	@Test
	public void checkThatAllScoresAreEnteredForRounds() {
		currentRound = 1;
		scoresEntered = 0;
	}

	@Test
	public void testWriteToFixtures() {

	}

	@Test
	public void testMatchPoints() {
		fail("Not yet implemented");
	}

	@Test
	public void testCalculateBonusPoints() {
	}

	@Test
	public void testReadFromFixturesIntInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testRemoveDuplicateScores() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsMatchEntered() {
		fail("Not yet implemented");
	}

	@Test
	public void testGenerateTable() {
		fail("Not yet implemented");
	}

	@Test
	public void testCheckGrandSlam() {
		fail("Not yet implemented");
	}
	@Test
	public void testLossPercentage() {

	}

}
