package master;

import static org.junit.Assert.*;

import java.sql.*;
import java.util.Arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Test_Fixtures 
{

	// Test data.
	Integer[][] testFixtures;	
	private static String expectedOutput;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
		
		Fixtures.DEBUG_MODE = true;
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
		
		Fixtures.DEBUG_MODE = false;
		
	}

	@Before
	public void setUp() throws Exception 
	{
	}

	@After
	public void tearDown() throws Exception 
	{		
	}

	@Test
	public void testGenerateFixtures() 
	{
				
		// Set the TEST_ID.
		Fixtures.TEST_ID = 1;
	
		// View the fixtures.
		testFixtures = Fixtures.generateFixtures();

		assertNotNull(testFixtures);
		
	}

	@Test
	public void testCheckMatchTrue() 
	{
		
		// Set the TEST_ID.
		Fixtures.TEST_ID = 2;
		
		Integer[][] testFixtures = {{1, 2}, {3, 4}};
		Integer[] testMatch = {1, 2};
		
		assertTrue(Fixtures.checkMatch(testFixtures, testMatch));
		
	}
		
	@Test
	public void testCheckMatchFalse() 
	{
	
		// Set the TEST_ID.
		Fixtures.TEST_ID = 3;
		
		Integer[][] testFixtures = {{1, 2}, {3, 4}};
		Integer[] testMatch = {2, 3};
		
		assertFalse(Fixtures.checkMatch(testFixtures, testMatch));
		
	}

	@Test
	public void testReverseMatch() 
	{
		
		// Set the TEST_ID.
		Fixtures.TEST_ID = 4;
		
		Integer[] testMatch = {1, 2};
		Integer[] testReversed = {2, 1};
				
		assertTrue(Arrays.deepEquals(testReversed, Fixtures.reverseMatch(testMatch)));
				
	}

	@Test
	public void testPushFixturesToDB() 
	{
	
		// Set the TEST_ID.
		Fixtures.TEST_ID = 5;
		
		expectedOutput = "Pushing fixtures!";
		
		testFixtures = Fixtures.fixtures;
		Fixtures.pushFixturesToDB(testFixtures);
		
		assertEquals(expectedOutput, Fixtures.DEBUG_STRING);
		
		// TODO(alistair): is this the right way to handle connections? don't know if this will work!
				
	}

	@Test
	public void testViewFixtures() 
	{
	
		// Set the TEST_ID.
		Fixtures.TEST_ID = 6;
		
		// We will expect this to be the value of Menu.DEBUG_STRING if the code
		// performs as intended.
		expectedOutput = "Viewing fixtures!";
		
		// View the fixtures.
		Fixtures.viewFixtures();

		// Has the debug string been set?
		// If so, the test will pass.
		assertEquals(expectedOutput, Fixtures.DEBUG_STRING);
	
	}

}
