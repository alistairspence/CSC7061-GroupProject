package master;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * The Fixtures class contains methods for generating and viewing fixtures.
 * The fixtures are randomly generated and guarantee that no team will play each other more than once.
 * @author <i>Alistair Spence
 * @author Caolan Daly
 * @author Cathal McNulty
 * @author John Corry
 * @author Niall Logue</i>
 * @see Test_Fixtures.java
 */
public class Fixtures 
{
	
	// TOTAL_MATCHES: sets the total amount of matches that are allowed to be generated.
	// In this case it is 5 rounds of 3 matches each, so 15 altogether.
	private static final int TOTAL_MATCHES = 15;
	
	// TEAMS_PER_MATCH: sets the number of teams per match, for use when creating the fixtures array.
	private static final int TEAMS_PER_MATCH = 2;

	// fixtures: Integer[][] containing 15 Integer[] arrays, each with an Integer pair signifying a match.
	// For example, Wales vs. Scotland would be signified by an Array containing [5, 4]. 
	// (these team values are set later)
	protected static Integer[][] fixtures = new Integer[TOTAL_MATCHES][TEAMS_PER_MATCH];
	
	// TOTAL_ROUNDS: sets the amount of rounds to be generated.
	private static final int TOTAL_ROUNDS = 5;
	
	// MATCHES_PER_ROUND: sets the number of matches in each round, to be used when printing to console.
	private static final int MATCHES_PER_ROUND = 3;
	
	// FIXTURES_VIEW_TIME: this sets how long the thread will sleep for after the fixtures are displayed.
	private static final int FIXTURES_VIEW_TIME = 4000;

	// RND: Random object used to draw teams.
	private static final Random RND = new Random();

	// MATCH_INDEX: this is a persistent int containing the value of the last match to be written.
	// It is used to generate the final fixture list.
	private static int MATCH_INDEX = 0;
		
	/*
	 * TESTING:
	 * 		The following variables are used only for making the test cases more manageable.
	 * 		Combining these variables allows user input to be simulated, therefore allowing the test cases
	 * 		to run at a far greater speed. Please see later code for implementation details.
	 * 
	 */
	
	// DEBUG_MODE: this boolean is only used when JUnit testing, and allows for easier
	// testing assertions. See later code for details.
	public static boolean DEBUG_MODE;
	
	// TEST_ID: used in conjunction with DEBUG_MODE to identify specific paths within the 
	// program, allowing for easier testing assertions. See later code for details.
	public static int TEST_ID;
	
	// DEBUG_STRING: this string is set to a debug message which are then tested against
	// within each test case. 
	public static String DEBUG_STRING;
	
	/**
	 * generateFixtures(): randomly generates 5 rounds of 3 matches each, where each team will not play each other
	 * more than once, and assigns them to an Integer[][].
	 * @return fixtures: Integer[][] containing the fixtures.
	 */
	protected static Integer[][] generateFixtures()
	{
		
		// Check to see if the fixtures have already been created.
		if (fixtures[0][0] == null)
		{			
				
			// If not, they are generated.
			// This for loop cycles through the rounds.
			for (int round = 0; round < TOTAL_ROUNDS; round++)
			{
						
				// Each match is stored in an array.
				Integer[] match1 = new Integer[TEAMS_PER_MATCH];
				Integer[] match2 = new Integer[TEAMS_PER_MATCH];
				Integer[] match3 = new Integer[TEAMS_PER_MATCH];
				
				// This arraylist is simply used to make conversion simpler down the line.
				ArrayList<Integer> currentRoundArrayList = new ArrayList<Integer>();
	
				// Teams are represented by these ints, and chosen one-by-one at random.
				int england = 0;
				int france = 1;
				int ireland = 2;
				int italy = 3;
				int scotland = 4;
				int wales = 5;
				
				// These booleans will be set to true when a team is drawn,
				// to prevent that team from being drawn twice in the same round.
				boolean englandPicked = false;
				boolean francePicked = false;
				boolean irelandPicked = false;
				boolean italyPicked = false;
				boolean scotlandPicked = false;
				boolean walesPicked = false;
				
				// This will be set to true when all teams have been picked.
				boolean allTeamsPicked = false;
	
				// The current round is cleared each time through the loop, ready for the new round.
				currentRoundArrayList.clear();
	
				// This while loop will cycle until all teams have been picked.
				// (i.e. a full round has been generated)
				while (!allTeamsPicked)
				{
					
					// Pick a team at random.
					int currentTeam = RND.nextInt(6);
	
					// Switch on that team.
					switch (currentTeam)
					{
	
					case 0:
	
						// In each case, validate the team hasn't been picked before.
						// If it has, pick again.
						if (englandPicked)
						{
	
							break;
	
						}
	
						// If not, the corresponding boolean is set to true and 
						// the int representing the team is added to the array.
						else
						{	
	
							englandPicked = true;
							currentRoundArrayList.add(england);
							break;
	
						}
	
					case 1:
	
						if (francePicked)
						{
	
							break;
	
						}
	
						else
						{
	
							francePicked = true;
							currentRoundArrayList.add(france);
							break;
	
						}
	
					case 2:
	
						if (irelandPicked)
						{
	
							break;
	
						}
	
						else 
						{
	
							irelandPicked = true;
							currentRoundArrayList.add(ireland);
							break;
	
						}
	
					case 3:
	
						if (italyPicked)
						{
	
							break;
	
						}
	
						else
						{
	
							italyPicked = true;
							currentRoundArrayList.add(italy);
							break;
	
						}
	
					case 4:
	
						if (scotlandPicked)
						{
	
							break;
	
						}
	
						else
						{
	
							scotlandPicked = true;
							currentRoundArrayList.add(scotland);
							break;
	
						}
	
					case 5:
	
						if (walesPicked)
						{
	
							break;
	
						}
	
						else
						{
	
							walesPicked = true;
							currentRoundArrayList.add(wales);
							break;
	
						}
	
					default:
						System.out.println("Something went wrong - please try again.");
						break;
	
					} // End switch statement.
	
					// Check to see if all teams have been picked - if so we break out of the loop
					if (englandPicked && francePicked && irelandPicked
							&& italyPicked && scotlandPicked && walesPicked)
					{
	
						allTeamsPicked = true;
	
					} // End if statement.
	
				} // End while loop.
	
				// Fetch the match values from the arraylist
				match1[0] = currentRoundArrayList.get(0);
				match1[1] = currentRoundArrayList.get(1);
	
				match2[0] = currentRoundArrayList.get(2);
				match2[1] = currentRoundArrayList.get(3);
	
				match3[0] = currentRoundArrayList.get(4);
				match3[1] = currentRoundArrayList.get(5);	
				
				// This checks to see if the fixtures array contains any matches that have been 
				// generated before (including reversed matches)
				// If so, the round is scrapped and redone
				if (checkMatch(fixtures, match1) || checkMatch(fixtures, match2) || checkMatch(fixtures, match3))
				{
					
					round--;
					continue;
								
				}
			
				// If not, the matches are written to the fixtures array
				// (MATCH_INDEX starts at 0 and is incremented a total of 3 times
				// per valid round)
				else
				{
					
					fixtures[MATCH_INDEX] = match1;
					MATCH_INDEX++;
					fixtures[MATCH_INDEX] = match2;
					MATCH_INDEX++;
					fixtures[MATCH_INDEX] = match3;
					MATCH_INDEX++;
					
				} 
				
			} // End for loop.
			
			// Return the resulting array
			return fixtures;
			
		}
	
		// If the fixtures have already been created, simply return them.
		else
		{
			
			return fixtures;
			
		}
		
	} // End of generateFixtures() method.
	
	/**
	 * checkMatch(): takes a particular 'match' (Integer[]) and iterates through the fixtures array
	 * to see if the match has already been generated.
	 * @param fixtures: Integer[][] containing all fixtures.
	 * @param match: Integer[] containing match to check for.
	 * @return true/false, depending on the result.
	 */
	public static boolean checkMatch(Integer[][] fixtures, Integer[] match)
	{
		
		// Generates a reversedMatch to test for alongside the original match.
		Integer[] reverseMatch = reverseMatch(match);
		
		// For each fixture in the fixtures array, test against the input match.
		for (Integer[] currentMatch : fixtures)
		{
			
			// If the match is already contained, return true.
			if (Arrays.deepEquals(currentMatch, match) || Arrays.deepEquals(currentMatch, reverseMatch))
			{
				
				return true;
				
			} // End of if statement.
						
		} // End of for loop.
		
		// If the match is not contained, return false.
		return false;
		
	} // End of checkMatch() method.
	
	/**
	 * reverseMatch(): takes a match and reverses its teams, then returns the match.
	 * @param match: Integer[] containing ints representing team 1 and team 2.
	 * @return reversed: Integer[] containing the reverse of match.
	 */
	public static Integer[] reverseMatch(Integer[] match)
	{
		
		// Create the integer to always be the same size as the input match.
		// (Should always be 2, but just to be safe)
		Integer[] reversed = new Integer[match.length];
		
		// Assign the teams to the correct reversed positions.
		reversed[0] = match[1];
		reversed[1] = match[0];
		
		// Return the Integer[].
		return reversed;
		
	} // End of reverseMatch() method.
	
	/**
	 * This method interfaces with the DBHelper class, and prepares the 
	 * fixtures to write to the database.
	 * @param fixtures: Integer[][] containing fixtures to write.
	 */
	public static void pushFixturesToDB(Integer[][] fixtures)
	{
		
		// TEST CODE
		if (DEBUG_MODE && (TEST_ID == 5))
		{
			
			DEBUG_STRING = "Pushing fixtures!";
			return;
			
		}
		
		// Iterate through the fixtures and write each to the database.
		// They are written in the format - match_id, team1_id, team2_id.
		for (int match = 0; match < TOTAL_MATCHES; match++)
		{
			
			DBHelper.writeToFixtures((match + 1), fixtures[match][0], fixtures[match][1]);
			
		} // End for loop.
		
	} // End of pushFixturesToDB() method.
	
	/**
	 * viewFixtures(): reads from the fixtures array and prints to console with formatting.
	 */
	protected static void viewFixtures()
	{
	
		// matchCounter/roundCounter: used to keep track of the current match/round.
		int matchCounter = 0;
		int roundCounter = 0;
		
		// Iterates through each fixture (an array containing two values, i.e [0, 4] or [3, 1])
		// and prints the details to the console.
		for (Integer[] fixture : fixtures)
		{
			
			// teamCounter: keeps track of whether it is team 1 (0) or team 2 (1) that is being printed.
			// It is reinitialised to 0 (i.e. Team 1) at the start of every loop.
			int teamCounter = 0;
						
			// If it is the first time through the loop, the round header is printed.
			if (matchCounter == 0)
			{
				
				System.out.println();
				System.out.println("\t---------");
				System.out.println("\t Round " + (roundCounter + 1));
				System.out.println("\t---------");
				System.out.println();
				
				// roundCounter is incremented to 1, to signify moving to the next round.
				roundCounter++;
				
			} // End if statement.
			
			
			// This will print the round header after every third match.
			else if (matchCounter > 0 && matchCounter % MATCHES_PER_ROUND == 0)
			{
				
				System.out.println();
				System.out.println("\t---------");
				System.out.println("\t Round " + (roundCounter + 1));
				System.out.println("\t---------");
				System.out.println();
				
				// Increments every time a new round begins.
				roundCounter++;
				
			} // End if statement.
			
			// TEST CODE
			if (DEBUG_MODE && (TEST_ID == 6))
			{
				
				DEBUG_STRING = "Viewing fixtures!";
				return;
				
			}
			
			else
				
			{
				
				// Cycle through team 1 and team 2 for each fixture, to assign a string for printing.
				for (Integer team : fixture)
				{
					
					// If this is the first team in a particular match, the prespacing is formatted.
					// This means all the fixtures will be centrally aligned.
					if (teamCounter == 0)
					{
						
						// Initialise preSpacing.
						String preSpacing = "";
						
						// Get the length of the team name currently being printed.
						int teamNameLength = (DBHelper.getTeamName(team).length());

						// Assign the correct preSpacing size to the string.
						if (teamNameLength == 8)
						{
							
							preSpacing = "   ";
							
						}
			
						else if (teamNameLength == 7)
						{
							
							preSpacing = "    ";
							
						}
						
						else if (teamNameLength == 6)
						{
							
							preSpacing = "     ";
							
						}
						
						else if (teamNameLength == 5)
						{
							
							preSpacing = "      ";
							
						}
						
						else
						{
							
							preSpacing = "   ";
							
						}
						
						System.out.print(preSpacing);
						
					}
						
					// Print appropriate team based on int.
					switch (team)
					{
					
					case 0:
						System.out.print("England ");
						break;
						
					case 1:
						System.out.print("France ");
						break;
						
					case 2:
						System.out.print("Ireland ");
						break;
						
					case 3:
						System.out.print("Italy ");
						break;
						
					case 4:
						System.out.print("Scotland ");
						break;
						
					case 5:
						System.out.print("Wales ");
						break;
						
					default:
						System.out.println("Something went wrong - please try again.");
						break;
					
					} // End switch statement.
					
					// Increment teamCounter to 1, this will only last until the next time through the loop, where it is reset to 0.
					teamCounter++;
					
					// Print out the vs. if necessary (i.e, team 1 has just been printed).
					if (teamCounter == 1)
					{
						
						System.out.print("- ");
						
					} // End if statement.
					
				} // End for loop.	
				
			}
			
			System.out.print("\n");
			
			// Increment matchCounter.
			matchCounter++;
			
		} // End for loop iterating through fixtures.
		
		System.out.println();
		
		// This try block will catch InterruptedExceptions.
		try
		{
			
			// The thread will sleep for the length of time specified by FIXTURES_VIEW_TIME.
			Thread.sleep(FIXTURES_VIEW_TIME);
			
		} catch (InterruptedException interrupt)
		{
			
			interrupt.printStackTrace();
			
		} // End catch statement.
	
	} // End of viewFixtures() method.

} // End of Fixtures class.
