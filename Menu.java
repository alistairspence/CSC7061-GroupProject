package master;

import java.util.Scanner;
import java.sql.SQLException;
import java.util.InputMismatchException;

/**
 * The Menu class contains the main method, which
 * automatically carries out database initialization management
 * and behind-the-scenes setup, before displaying the main menu.
 * It also contains methods relating to the menu options.
 * @author <i>Alistair Spence
 * @author Caolan Daly
 * @author Cathal McNulty
 * @author John Corry
 * @author Niall Logue</i>
 * @see DBHelper.java, Test_Menu.java
 *
 */
public class Menu 
{
		
	// MENU_CHOICE_LOW: sets the lower bound for possible menu choices,
	// allowing more transparent detection of invalid input.
	private static final int MENU_CHOICE_LOW = 1;
	
	// MENU_CHOICE_HIGH: sets the upper bound for possible menu choices,
	// allowing more transparent detection of invalid input.
	private static final int MENU_CHOICE_HIGH = 8;
	
	// MENU_PAUSE_TIME: when the option to quit the program is selected, 
	// the thread sleeps for the length of time specified here (in ms) 
	// to simulate background close-down actions.
	private static final int MENU_PAUSE_TIME = 4000;
	
	// MENU_CLEAR_LINES: when the program quits and the thread is sleeping,
	// upon waking up blank lines are printed to clear the screen, simulating
	// the closing of the program. This value specifies how many lines are printed
	// and after quick testing, has been set to 50. This should be enough to clear
	// on most monitors.
	private static final int MENU_CLEAR_LINES = 50;
		
	// MIN_ROUND_CHOICE: used for validating within the viewRoundScores() and 
	// uploadRoundScores() methods.
	private static final int MIN_ROUND_CHOICE = 1;
	
	// MAX_ROUND_CHOICE: used for validating within the viewRoundScores() and 
	// uploadRoundScores() methods.
	private static final int MAX_ROUND_CHOICE = 5;
	
	// MIN_TEAM_CHOICE: used for validating within the viewPlayers() method.
	private static final int MIN_TEAM_CHOICE = 1;
	
	// MAX_TEAM_CHOICE: used for validating within the viewPlayers() method.
	private static final int MAX_TEAM_CHOICE = 6;

	// menuRunning: this boolean is set to true by default, and is used to cycle back
	// to displaying the menu when any of the menu options are selected and executed.
	// This will be set to false either when the quit option is selected, or if the program
	// encounters an error and must terminate.
	protected static boolean menuRunning;
	
	// user_input: this Scanner is used to handle user input regarding menu selection,
	// and selection of options beyond that point.
	private static Scanner user_input = new Scanner(System.in);
	
	/*
	 * TESTING:
	 * 		The following variables are used only for making the test cases more manageable.
	 * 		Combining these variables allows user input to be simulated, therefore allowing the test cases
	 * 		to run at a far greater speed. Please see later code for implementation details.
	 * 
	 */
	
	// DEBUG_MODE: this boolean is only used when JUnit testing, and allows for easier
	// testing assertions. See later code for details.
	public static boolean DEBUG_MODE;
	
	// TEST_ID: used in conjunction with DEBUG_MODE to identify specific paths within the 
	// program, allowing for easier testing assertions. See later code for details.
	public static int TEST_ID;
	
	// DEBUG_STRING: this string is set to a debug message which are then tested against
	// within each test case. 
	public static String DEBUG_STRING;

	/**
	 * main(): automatically carries out database initialization management
	 * and behind-the-scenes setup, before displaying the main menu.
	 * 
	 */
	public static void main(String[] args)
	{
		
		// Initialise the global connection within the DBHelper class.
		DBHelper.getRemoteConnection();
		
		// SIMPLY FOR DEBUG PURPOSES!
		// DBHelper.clearDB();
		
		// Check to see if the database is initialised. If not, initialise it.
		if (!DBHelper.isDBInitialised())
		{
	
			// Write the teams into the database & inform the user.
			System.out.println("Retrieving teams..");
			System.out.println("Writing teams to database..");
			DBHelper.writeToTeams();
			
			// Generate the fixtures, write them to the database & inform the user.
			System.out.println("Generating fixtures..");
			Fixtures.fixtures = Fixtures.generateFixtures();
			
			System.out.println("Writing fixtures to database..\n");
			Fixtures.pushFixturesToDB(Fixtures.fixtures);
			
		} // End if statement.
		
		// The displayMenu() method is then finally called.
		// The program will not return to the main method until the quit
		// option has been selected.
		displayMenu();
		
	} // End of main method.
	
	/**
	 * displayMenu(): prints the menu to screen and contains code for handling & passing on
	 * user input.
	 */
	public static void displayMenu()
	{
	
		// The menu is set to running.
		menuRunning = true;
		
		// The menu will be displayed until menuRunning is set to false.
		// This will only happen if the quit option is selected, or if a fatal error occurs.
		while (menuRunning)
		{
			
			// Formatting the menu.
			System.out.println("|       Six Nations       |");
			System.out.println("---------------------------");
			System.out.println();
			System.out.println("  1. View fixtures");
			System.out.println("  2. Enter match scores");
			System.out.println("  3. View round scores");
			System.out.println("  4. Upload round scores");
			System.out.println("  5. View league table");
			System.out.println("  6. Search");
			System.out.println("  7. View players");
			System.out.println("  8. Quit");
			System.out.println();
			System.out.println("---------------------------");
			
			// TEST CODE
			if (DEBUG_MODE && (TEST_ID == 1))
			{
				
				// DEBUG_STRING is updated, and the method is exited.
				DEBUG_STRING = "Menu displayed!";
				return;
				
			}
		
			// This try block catches either an InputMismatchException, or a MenuChoiceException.
			// This protected against both invalid input (ie. char/string input) and values outside 1-7, as these 
			// do not correspond to a menu option.
			try 
			{
			
				// menuChoice: tracks the option selected by the user.
				int menuChoice = -1;
				
				// Prompt the user for their choice.
				System.out.print("Please select an option: ");
				
				// TEST CODE: if in debug mode, the menu choice is set according to the test that 
				// is being run. otherwise, it is set to accept user input.
				if (DEBUG_MODE)
				{
					
					// This sets the menuChoice to the option required.
					switch (TEST_ID)
					{
					
					// Testing invalid input (low).
					case 2:
						menuChoice = 0;
						break;
						
					// Testing invalid input (high).
					case 3:
						menuChoice = 9;
						break;
						
					// Testing string entry. (menuChoice cannot be set to a string - however the exception
					// would still be handled by the try-catch below)
					case 4:
						menuChoice = Integer.parseInt("String");
						break;
						
					// Testing viewFixtures.
					case 5:
						menuChoice = 1;
						break;
						
					// Testing enterMatchScores.
					case 6:
						menuChoice = 2;
						break;
						
					// Testing viewRoundScores.
					case 7:
					case 8:
					case 9:
					case 10:
						menuChoice = 3;
						break;
						
					// Testing uploadRoundScores.
					case 11:
					case 12:
					case 13:
					case 14:
						menuChoice = 4;
						break;
						
					// Testing viewTable.
					case 15:
						menuChoice = 5;
						break;
						
					// Testing searchDB.
					case 16:
						menuChoice = 6;
						break;
						
					// Testing viewPlayers.
					case 17:
					case 18:
					case 19:
					case 20:
						menuChoice = 7;
						break;
						
					// Testing quit.
					case 21:
						menuChoice = 8;
						break;
						
					default:
						break;
					
					} // End switch statement.
					
				} // End DEBUG_MODE if statement.
					
				else 
				{
				
					// menuChoice is set to the user's input.
					menuChoice = user_input.nextInt();
				
				}
				
				// Pass the value on only if it is a valid choice. If not, a MenuChoiceException is thrown.
				if (menuChoice >= MENU_CHOICE_LOW && menuChoice <= MENU_CHOICE_HIGH)
				{
											
					// This switch statement handles calling the method which corresponds to the user's choice.
					switch (menuChoice)
					{
						
					case 1:
						
						// TEST CODE
						if (DEBUG_MODE && (TEST_ID == 5))
						{
							
							DEBUG_STRING = "Viewing fixtures!";
							return;
							
						}
						
						// If not testing, execute as normal.
						else
						{
						
							Fixtures.viewFixtures();							
						
						}
						
						break;
							
					case 2:
						
						// TEST CODE
						if (DEBUG_MODE && (TEST_ID == 6))
						{
							
							DEBUG_STRING = "Entering scores!";
							return;
							
						}
						
						// If not testing, execute as normal.
						else
						{
							
							DBHelper.writeToScores();
						
						}
						
						break;
							
					case 3:
						viewRoundScores();
						break;
							
					case 4:
						uploadRoundScores();
						break;
							
					case 5:
						
						// TEST CODE
						if (DEBUG_MODE && (TEST_ID == 15))
						{
							
							DEBUG_STRING = "Viewing table!";
							return;
							
						}
						
						// If not testing, execute as normal.
						else
						{
							
							DBHelper.generateTable();
						
						}
						
						break;
							
					case 6:
						
						// TEST CODE
						if (DEBUG_MODE && (TEST_ID == 16))
						{
							
							DEBUG_STRING = "Searching!";
							return;
							
						}
						
						// If not testing, execute as normal.
						else
						{
							
							DBHelper.searchDB();
							
						}
						
						break;
							
					case 7:
						viewPlayers();
						break;
						
					case 8:	
						quit();
						break;
							
					// This should never happen - invalid input is handled before reaching this switch statement.
					// It should never have to deal with values outside of the 1-7 range.
					default:
						System.out.println("Should not be here!");
						break;	
						
					} // End of the switch statement.
						
					// TEST CODE
					// 	  (prevents infinite menu loop)
					if (DEBUG_MODE)
					{
						
						switch (TEST_ID)
						{
						
						case 7:
						case 8:
						case 9:
						case 10:
						case 11:
						case 12:
						case 13:
						case 14:
						case 17:
						case 18:
						case 19:
						case 20:
						case 21:
							return;
							
						default:
							break;
						
						} // End of switch statement.
						
					} // End DEBUG_MODE if statement.
						
				} // End of the if statement validating the menuChoice.
						
				// A MenuChoiceException is thrown if the value is an integer, but outside of the valid range.
				else
				{
						
					menuRunning = false;
					throw new MenuChoiceException();
						
				}
					
			} catch (MenuChoiceException | InputMismatchException menuInput)
			{
				
				// TEST CODE
				if (DEBUG_MODE)
				{
					
					switch (TEST_ID)
					{
					
					case 2:
						DEBUG_STRING = "Invalid menu choice - too low!";
						return;
						
					case 3:
						DEBUG_STRING = "Invalid menu choice - too high!";
						return;
											
					default:
						break;
					
					} // End of switch statement.
					
				} // End DEBUG_MODE if statement.
				
				// Prompt the user to select a valid option.
				System.out.println("\nPlease select a valid option.\n");
				
				// This clears the invalid input from the Scanner. Otherwise,
				// the invalid input would be cycled through the menu again and again
				// in an infinite loop.
				user_input.next();
				
				// This try block will catch InterruptedExceptions.
				try
				{
					
					// The thread is put to sleep for the time specified by MENU_PAUSE_TIME, before the user is prompted to enter a choice again.
					Thread.sleep(MENU_PAUSE_TIME);
					
				} catch (InterruptedException interrupt)
				{
					
					menuRunning = false;
					interrupt.printStackTrace();
					
				} // End of InterruptedException catch statement.
				
			} // End of MenuChoiceException/InputMismatchException catch statement.
			
		} // End of menu while loop.
		
	} // End of displayMenu() method.

	/**
	 * viewRoundScores(): calls the readFromScores() method found in the DBHelper class
	 * with a valid round choice.
	 */
	private static void viewRoundScores()
	{
	
		try
		{
		
			// Prompt the user for a selection and store it.
			System.out.print("Which round would you like to view? ");
			
			// The user's choice is initialised out of range.
			int roundChoice = -1;
			
			// TEST CODE: if in debug mode, the round choice is set according to the test that 
			// is being run. otherwise, it is set to accept user input.
			if (DEBUG_MODE)
			{
				
				switch (TEST_ID)
				{
				
				case 7:
					roundChoice = 3;
					break;
					
				case 8:
					roundChoice = 0;
					break;
					
				case 9:
					roundChoice = 6;
					break;
					
				case 10:
					roundChoice = Integer.parseInt("String");
					break;
				
				} // End of switch statement.
				
			} // End DEBUG_MODE if statement.
			
			// If DEBUG_MODE is false, proceed as normal:
			else
			{
			
				roundChoice = user_input.nextInt();
	
			}
			
			// If the user has chosen an appropriate value, the readFromScores() method 
			// in the DBHelper class is called.
			if (roundChoice >= MIN_ROUND_CHOICE && roundChoice <= MAX_ROUND_CHOICE)
			{

				if (DEBUG_MODE && (TEST_ID == 7))
				{
					
					DEBUG_STRING = "Viewing round!";
					return;
					
				} // End if statement.
				
				// Call the corresponding method based on the user's selection.
				DBHelper.readFromScores(roundChoice);
				
			} // End if statement.
			
			// If the value is outside the range, the user is prompted to try again.
			else
			{

				if (DEBUG_MODE && (TEST_ID == 8))
				{
					
					DEBUG_STRING = "Invalid round choice - too low!";
					return;
					
				}
				
				else if (DEBUG_MODE && (TEST_ID == 9))
				{
					
					DEBUG_STRING = "Invalid round choice - too high!";
					return;
					
				}
				
				System.out.println("Please choose a valid round.");
				
			}
				
		} catch (InputMismatchException roundChoice)
		{
					
			System.out.println("Please enter a valid selection.");
			
		}
			
	} // End of viewRoundScores() method.
	
	/**
	 * uploadRoundScores(): checks to see if the program is running on Mac, and if so, calls the 
	 * uploadRoundScoresMac() method within the DBHelper class to upload the scores.
	 * If not, it calls the uploadRoundScoresWindows() method.
	 * 
	 * The methods must be separated due to the handling of SQL queries on either system.
	 */
	protected static void uploadRoundScores()
	{
		
		String system = System.getProperty("os.name");
		
		try
		{
		
			// Prompt the user for input and store it.
			System.out.print("Which round would you like to upload? ");
			int round = -1;
			
			// TEST CODE: if in debug mode, the round choice is set according to the test that 
			// is being run. otherwise, it is set to accept user input.
			if (DEBUG_MODE)
			{
				
				switch (TEST_ID)
				{
				
				case 11:
					round = 3;
					break;
					
				case 12:
					round = 0;
					break;
					
				case 13:
					round = 6;
					break;
					
				case 14:
					round = Integer.parseInt("String");
					break;
					
				default:
					break;
				
				} // End switch statement.
				
			} // End DEBUG_MODE if statement.
			
			else
			{
				
				round = user_input.nextInt();
				
			}
			
			// Validation of the round choice is done here.
			if (round >= MIN_ROUND_CHOICE && round <= MAX_ROUND_CHOICE)
			{
			
				// TEST CODE
				if (DEBUG_MODE && (TEST_ID == 11))
				{
					
					DEBUG_STRING = "Round scores uploaded!";
					return;
					
				}
				
				else 
				{
					
					// If the system running the program is a Mac, the correct method is called.
					if (system.equals("Mac OS X"))
					{
						
						DBHelper.uploadRoundScoresMac(round);
						
					}
					
					// If the system running the program is Windows, the correct method is called.
					else if (system.contains("Windows"))
					{
						
						DBHelper.uploadRoundScoresWindows(round);
						
					} // End system if statement.
					 
				} // End DEBUG_MODE if statement.
				
			} // End round validation if statement.
			
			else
			{
				
				// TEST CODE
				if (DEBUG_MODE && (TEST_ID == 12))
				{
					
					DEBUG_STRING = "Invalid round choice - too low!";
					return;
					
				}
				
				// TEST CODE
				else if (DEBUG_MODE && (TEST_ID == 13))
				{
					
					DEBUG_STRING = "Invalid round choice - too high!";
					return;
					
				}
				
				// Prompt the user to try again.
				System.out.println("Please choose a valid round.");
				
			}
		
		} catch (InputMismatchException roundChoice)
		{
			
			System.out.println("Please enter a valid selection.");
			
		}
			
	} // End of uploadRoundScores() method.
	
	/**
	 * viewPlayers(): calls the Teams.printPlayers() method and passes
	 * the id of the team being selected.
	 */
	private static void viewPlayers()
	{
	
		// Formatting of the menu choice.
		System.out.println();
		System.out.println("1. England\t4. Italy");
		System.out.println("2. France \t5. Scotland");
		System.out.println("3. Ireland\t6. Wales");
		System.out.print("Which team would you like to view? ");
		
		try 
		{
		
			// Initialise user choice outside of range.
			int userChoice = -1;
			
			// TEST CODE: if in debug mode, the round choice is set according to the test that 
			// is being run. otherwise, it is set to accept user input.
			if (DEBUG_MODE)
			{
				
				switch (TEST_ID)
				{
				
				case 17:
					userChoice = 3;
					break;
					
				case 18:
					userChoice = 0;
					break;
					
				case 19:
					userChoice = 7;
					break;
					
				case 20:
					userChoice = Integer.parseInt("String");
					break;
					
				default:
					break;
				
				}
				
			}
			
			else 
			{
			
				userChoice = user_input.nextInt();
			
			}
			
			// Validation of the user choice is done here.
			if (userChoice >= MIN_TEAM_CHOICE && userChoice <= MAX_TEAM_CHOICE)
			{
					
				// Team ID's are stored as 0-5, so the user's choice is mapped by 
				// decrementing by one. 
				userChoice--;
				
				// TEST CODE
				if (DEBUG_MODE && (TEST_ID == 17))
				{
					
					DEBUG_STRING = "Viewing players!";
					return;
					
				}
				
				else
				{
					
					Teams.printPlayers(userChoice);
				
				}
				
			}
			
			else 
			{
				
				// TEST CODE
				if (DEBUG_MODE && (TEST_ID == 18))
				{
					
					DEBUG_STRING = "Invalid team choice - too low!";
					return;
					
				}
				
				else if (DEBUG_MODE && (TEST_ID == 19))
				{
					
					DEBUG_STRING = "Invalid team choice - too high!";
					return;
					
				}
				
				System.out.println("Please choose a valid team.");
				
			}
		
		} catch (InputMismatchException teamChoice)
		{

			System.out.println("Please enter a valid selection.");
			
		}
		
	}
	
	/**
	 * quit(): contains code to stop the menu loop, sleep the thread and clear the console screen.
	 */
	private static void quit()
	{
						
		// This will cause the menu to stop looping.
		menuRunning = false;
		
		// Lets the user know the program is exiting.
		System.out.println("Quitting..");
		
		// TEST CODE
		if (DEBUG_MODE && (TEST_ID == 21))
		{
			
			DEBUG_STRING = "Quitting!";
			return;
			
		}
		
		// This try block will catch InterruptedExceptions.
		try
		{
			
			// The thread is put to sleep for the time specified by MENU_PAUSE_TIME, before the screen is cleared.
			Thread.sleep(MENU_PAUSE_TIME);
			
		} catch (InterruptedException interrupt)
		{
			
			interrupt.printStackTrace();
			
		} // End of InterruptedException catch statement.
		
		// This will print enough blank lines to clear the user's console. The value is set by the constant MENU_CLEAR_LINES.
		
		for (int blankLine = 0; blankLine < MENU_CLEAR_LINES; blankLine++)
		{
		
			System.out.println();
			
		} // End of for loop.
							
	} // End of quit() method.
	
} // End of Menu class.
